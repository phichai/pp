package com.RetailSoft.shop_op;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.RetailSoft.shop_op.util.BitmapUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;





public class TaskDetail extends Activity {
	protected static final int TAKE_PICTURE = 1;
	private TaskDetail mContext;
	private ImageView mCmBtn;
	protected Uri imageUri;
	private BitmapUtil mBmUtil;

	private SharedPreferences prefs;
	private String defBg;
	private String shopid;
	private String mOperationFeq;
	private String mOperationType;
	private String mTaskname;
	private String mTaskid;
	private String mTasktrans;
	private TextView mTaskText;
	private EditText mChklistTag;

	private RadioButton mGotListNo;
	private RadioButton mListPassYes;
	private RadioButton mListPassNo;
	private EditText mChkListDetail;
	private Button mSubmitBtn;

	private String listpass;
	protected Uri mCapturedImageURI;
	public File fileToUpload;
	private Bitmap bm;

	private String encodedImg;
	private String userid;
	private Bitmap blurImg;
	private Button mTboxbtn;
	private CheckBox mTboxChk;
	private RadioGroup mListGroup;
	

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.chklist_detail);
		mContext = this;
		mBmUtil = new BitmapUtil();


		prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
		
		shopid = prefs.getString("shopid", "");
		mOperationFeq = prefs.getString("defFOPT", "M");
		mOperationType = prefs.getString("defTOPT", "M");
		userid = prefs.getString("userLogin", "");

		Intent mIntent = getIntent();
		mTaskname = mIntent.getStringExtra("taskname");
		mTaskid = mIntent.getStringExtra("masterid");
		mTasktrans = mIntent.getStringExtra("tasktrans");

		bindwidget();
		setListener();

		
		DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder().build();
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext()).defaultDisplayImageOptions(defaultOptions).build();
		ImageLoader.getInstance().init(config); // Do it on Application start
		/**/
		if (mTasktrans.equalsIgnoreCase("null")) {

		} else {
			FetchDataTask mTasktransData = new FetchDataTask();
			mTasktransData.execute();
		}


		mTaskText.setText(mTaskname);
		blurImg = null;
		defBg = null;
		bm = null;
		
	}
	
	

	private void bindwidget() {
		// TODO Auto-generated method stub
		
		mCmBtn = (ImageView) findViewById(R.id.cmBtn);
		mTaskText = (TextView) findViewById(R.id.chklistname);
		mChklistTag = (EditText) findViewById(R.id.chklisttxt);
		
		mListGroup = (RadioGroup) findViewById(R.id.gotList);
		mGotListNo = (RadioButton) findViewById(R.id.gotListno);
		mListPassYes = (RadioButton) findViewById(R.id.listPassyes);
		mListPassNo = (RadioButton) findViewById(R.id.listPassno);

		mChkListDetail = (EditText) findViewById(R.id.chklistDetail);
		mSubmitBtn = (Button) findViewById(R.id.submitBtn);
		
		mTboxbtn = (Button) findViewById(R.id.tboxbtn); 
		mTboxChk = (CheckBox) findViewById(R.id.tboxchk);
	}

	private void setListener() {
		// TODO Auto-generated method stub

		mCmBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*
				Intent cameraIntent = new
				Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
				startActivityForResult(cameraIntent, TAKE_PICTURE); 
				/**/
				String fileName = "temp.jpg";
				ContentValues values = new ContentValues();
				values.put(MediaStore.Images.Media.TITLE, fileName);
				mCapturedImageURI = getContentResolver().insert(
						MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				intent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
				startActivityForResult(intent, TAKE_PICTURE);
			}
		});

		mGotListNo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				listpass = "0";
			}
		});

		mListGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				//Log.i("chk change",String.valueOf(checkedId));
				if(checkedId == R.id.listPassyes){
					listpass = "2";
					showTbox(false);			
				}else if(checkedId == R.id.listPassno){
					listpass = "1";
					showTbox(true);
				}else{
					listpass = "0";
					showTbox(true);
				}
			}
		});
		
		mTboxbtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				//Dialog dialog = new Dialog(mContext);
				Log.i("toast me","please");
				Toast toast= Toast.makeText(getApplicationContext(), 
						"This Feature Available on next version", Toast.LENGTH_SHORT);  
						toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 0, 0);
						toast.show();
			}
		});
		
		mSubmitBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				PostDataTask ptask = new PostDataTask();
				ptask.execute();
			}
		});
	}

	protected void showTbox(boolean b) {
		// TODO Auto-generated method stub
		if(b){
			mTboxbtn.setVisibility(View.VISIBLE);
			mTboxChk.setVisibility(View.VISIBLE);
		}else{
			mTboxbtn.setVisibility(View.GONE);
			mTboxChk.setVisibility(View.GONE);
		}
	}



	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case TAKE_PICTURE:
			if (resultCode == Activity.RESULT_OK) {

				/**/
				String[] projection = { MediaStore.Images.Media.DATA };
				Cursor cursor = managedQuery(mCapturedImageURI, projection,
						null, null, null);
				int column_index_data = cursor
						.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
				cursor.moveToFirst();
				String capturedImageFilePath = cursor
						.getString(column_index_data);

				ByteArrayOutputStream baos = new ByteArrayOutputStream();

				Bitmap thumbnail = getThumbnailBitmap(capturedImageFilePath,
						568);
				mCmBtn.setImageBitmap(thumbnail);

				thumbnail.compress(Bitmap.CompressFormat.JPEG, 60, baos);
				byte[] b = baos.toByteArray();
				encodedImg = Base64.encodeToString(b, Base64.DEFAULT);
				/**/
				// Log.i("path", capturedImageFilePath);
				thumbnail = null;
			}
		}
	}

	private Bitmap getThumbnailBitmap(String path, int thumbnailSize) {
		BitmapFactory.Options bounds = new BitmapFactory.Options();
		bounds.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(path, bounds);
		if ((bounds.outWidth == -1) || (bounds.outHeight == -1)) {
			return null;
		}
		int originalSize = (bounds.outHeight > bounds.outWidth) ? bounds.outHeight
				: bounds.outWidth;
		BitmapFactory.Options opts = new BitmapFactory.Options();
		opts.inSampleSize = originalSize / thumbnailSize;
		return BitmapFactory.decodeFile(path, opts);
	}

	class PostDataTask extends AsyncTask<String, Integer, Void> {

		public StringBuilder str;
		private DefaultHttpClient client;
		private HttpPost httpPost;
		private ProgressDialog loadingDialog;
		
		protected void onPreExecute() {
			// Log.i("FetchDataTask", "Start FetchDataTask");
			loadingDialog = ProgressDialog.show(mContext, "", "กำลังบันทึกข้อมูล",
					true);
			loadingDialog.setCancelable(true);
			loadingDialog.setOnCancelListener(new OnCancelListener() {
				@Override
				public void onCancel(DialogInterface dialog) {
					finish();
				}
			});

			loadingDialog.show();

		}
		@Override
		protected Void doInBackground(String... arg0) {
			List<NameValuePair> prams = new ArrayList<NameValuePair>();

			str = new StringBuilder();
			String url = "http://mshop.ssup.co.th/ros/upload.php";
			// Log.i("URL", url);
			client = new DefaultHttpClient();
			httpPost = new HttpPost(url);
			try {

				prams.add(new BasicNameValuePair("chklisttag", mChklistTag
						.getText().toString()));
				prams.add(new BasicNameValuePair("listpass", listpass));
				prams.add(new BasicNameValuePair("chklistcomment",
						mChkListDetail.getText().toString()));
				prams.add(new BasicNameValuePair("shopid", shopid));
				prams.add(new BasicNameValuePair("userid", userid));
				prams.add(new BasicNameValuePair("ofeq", mOperationFeq));
				prams.add(new BasicNameValuePair("otype", mOperationType));
				prams.add(new BasicNameValuePair("tname", mTaskname));
				prams.add(new BasicNameValuePair("mastertaskid", mTaskid));
				prams.add(new BasicNameValuePair("transtaskid", mTasktrans));
				prams.add(new BasicNameValuePair("imageupload", encodedImg));

				/**/
				// httpPost.setEntity(new StringEntity(body, HTTP.UTF_8));

				httpPost.setEntity(new UrlEncodedFormEntity(prams, HTTP.UTF_8));

				HttpResponse response = client.execute(httpPost);
				StatusLine statusLine = response.getStatusLine();

				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) { // Status OK
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						str.append(line);
					}
				} else {
					Log.e("Log", "Failed to connect server..");
				}

			} catch (ClientProtocolException e) {
				Log.i("ERR1", "Post fail");
				e.printStackTrace();
			} catch (IOException e) {
				Log.i("ERR2", "Data fail");
				e.printStackTrace();
			}finally{
				prams = null;
				url = null;
			}

			return null;
		}

		protected void onPostExecute(Void result) {
			Log.i("Post response",str.toString());
			str = null;
			httpPost = null;
			client = null;
			loadingDialog.dismiss();
			loadingDialog = null;
			Toast.makeText(mContext, "บันทึกข้อมูลเรียบร้อย", Toast.LENGTH_LONG)
					.show();
			mContext.startActivity(new Intent(mContext, OperationChecklist.class));
			finish();

		}

	}

	class FetchDataTask extends AsyncTask<String, Integer, Void> {
		public StringBuilder str;
		private DefaultHttpClient client;
		private HttpPost httpPost;
		private ProgressDialog loadingDialog;

		protected void onPreExecute() {
			// Log.i("FetchDataTask", "Start FetchDataTask");
			loadingDialog = ProgressDialog.show(mContext, "", "กำลังดึงข้อมูล",
					true);
			loadingDialog.setCancelable(true);
			loadingDialog.setOnCancelListener(new OnCancelListener() {
				@Override
				public void onCancel(DialogInterface dialog) {
					finish();
				}
			});

			loadingDialog.show();

		}

		@Override
		protected void onProgressUpdate(Integer... progress) {
			super.onProgressUpdate(progress[0]);
			loadingDialog.setProgress(progress[0]);
		}

		@Override
		protected Void doInBackground(String... arg0) {
			// TODO Auto-generated method stub
			List<NameValuePair> prams = new ArrayList<NameValuePair>();

			str = new StringBuilder();
			String url = "http://mshop.ssup.co.th/ros/tasktrans.php";
			client = new DefaultHttpClient();
			httpPost = new HttpPost(url);
			try {
				Log.i("POST trans", mTasktrans);
				prams.add(new BasicNameValuePair("transtaskid", mTasktrans));
				httpPost.setEntity(new UrlEncodedFormEntity(prams, HTTP.UTF_8));
				HttpResponse response = client.execute(httpPost);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) { // Status OK
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						str.append(line);
					}
				} else {
					Log.e("Log", "Failed to connect server..");
				}
			} catch (ClientProtocolException e) {
				Log.i("ERR1", "Post fail");
				e.printStackTrace();
			} catch (IOException e) {
				Log.i("ERR2", "Data fail");
				e.printStackTrace();
			}finally{
				prams = null;
				url = null;
				
			}
			return null;
		}

		protected void onPostExecute(Void result) {
			loadingDialog.dismiss();
			loadingDialog = null;
			try {
				JSONObject subObject = new JSONObject(str.toString());
				String chklist_transid = subObject.getString("chklist_transid");
				String chklist_id = subObject.getString("chklist_id");
				String chklist_name = subObject.getString("chklist_name");
				String user_id = subObject.getString("user_id");
				String chklist_reportname = subObject
						.getString("chklist_reportname");
				String shop_code = subObject.getString("shop_code");
				String chklist_pass = subObject.getString("chklist_pass");
				String chklist_desc = subObject.getString("chklist_desc");
				String period_time = subObject.getString("period_time");
				
				//String rosCheck
				String rosComment = subObject.getString("chklist_desc");

				mChklistTag.setText(chklist_reportname);
				if (chklist_pass.equalsIgnoreCase("2")) {
					mListPassYes.setChecked(true);
				} else if (chklist_pass.equalsIgnoreCase("1")) {
					mListPassNo.setChecked(true);
				} else {
					mGotListNo.setChecked(true);
				}
				if(!rosComment.isEmpty()){
					mChkListDetail.setHint(rosComment);
					
				}else{
					mChkListDetail.setText(chklist_desc);
				}
				
				//period_time = "2014-04-25";
				String url = "http://mshop.ssup.co.th/ros/upload/"
						+ shop_code + "/" + chklist_id + "/" + chklist_transid
						+ "/" + period_time + ".jpg";
				Log.i("img path", url);
				//imageLoader.loadImage(url, mCmBtn);
				ImageLoader.getInstance().displayImage(url, mCmBtn);
				
				subObject = null;

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				str = null;
				httpPost = null;
				client = null;
				loadingDialog = null;
			}
			
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			mContext.startActivity(new Intent(mContext, OperationChecklist.class));
			finish();
			
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
