 package com.RetailSoft.shop_op;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

 public class PlayVideo extends Activity
 {


  private String videoPath="http://112.121.152.34:80/stream/528c7edfa14fb/playlist.m3u8";
  private String url = "http://stream.ssup.co.th/ajax/onair.php?brand=";
  private String shopname="OP";
  private SharedPreferences prefs;
  private String user_id;
  private static ProgressDialog progressDialog;
  private Context context;
  String videourl;  
  VideoView videoView ;


  @SuppressLint("NewApi")
protected void onCreate(Bundle savedInstanceState)
  {

   super.onCreate(savedInstanceState);
   requestWindowFeature(Window.FEATURE_NO_TITLE);
   getWindow().setFlags(
       WindowManager.LayoutParams.FLAG_FULLSCREEN,  
        WindowManager.LayoutParams.FLAG_FULLSCREEN);
   setContentView(R.layout.play_video);
   context  = this;
   // Permission StrictMode
   if (android.os.Build.VERSION.SDK_INT > 9) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
   }
   
  
    videoView = (VideoView) findViewById(R.id.videoView);


    progressDialog = ProgressDialog.show(PlayVideo.this, "", "Buffering video...", true);
    progressDialog.setCancelable(true);  
    
    Bundle extras = getIntent().getExtras();
	   if(extras != null){
		   shopname = extras.getString("shopname");
		   user_id	= extras.getString("user_id");
		  
		   //Toast.makeText(this, "shop="+shopname.toString(), Toast.LENGTH_SHORT).show();
	   }   
   
	   List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("name", shopname));
	    url = url+shopname+"&user_id="+user_id;
	    Log.i("===Log url===",url);
		String resultServer  = getHttpPost(url,nameValuePairs);
		Log.i("===service===",resultServer);

		String  onair= "";
		JSONObject c;
		try {
			c = new JSONObject(resultServer);
			onair 		= c.getString("onair");
			videoPath 	= c.getString("full_url");
	    	//Toast.makeText(this, path.toString(), Toast.LENGTH_SHORT).show();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		if(onair.equals("Y")){
			  PlayVideo();
		}else{
			final AlertDialog.Builder ad = new AlertDialog.Builder(this);
			ad.setTitle("แจ้งเตือน");
			ad.setPositiveButton("ปิด", null);
			ad.setMessage("ไม่สามารถดูวีดีโอได้");
	        ad.show();
		}		



     

  }
  private void PlayVideo()
  {
   try
        {      
               
	   		getWindow().setFormat(PixelFormat.TRANSLUCENT);
               MediaController mediaController = new MediaController(PlayVideo.this);
               mediaController.setAnchorView(videoView);    
                Uri video = Uri.parse(videoPath.toString());             
                videoView.setMediaController(mediaController);
                videoView.setVideoURI(video);
                videoView.requestFocus();  
                videoView.start();
                
                videoView.setOnPreparedListener(new OnPreparedListener()
                {

                    public void onPrepared(MediaPlayer mp)
                    {                  
                        progressDialog.dismiss();     
                        videoView.start();
                    }
                });           

             }
        catch(Exception e)
        {
                 progressDialog.dismiss();
                 System.out.println("Video Play Error :"+e.toString());
                 finish();
        }   

  }
  public String getHttpPost(String url,List<NameValuePair> nameValuePairs) {
		StringBuilder str = new StringBuilder();
		HttpClient client = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(url);
		try {
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF8"));
			//httpPost.setHeader("Content-type", "application/json");
			HttpResponse response = client.execute(httpPost);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if (statusCode == 200) { // Status OK
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) {
					str.append(line);
				}
			} else {
				Log.e("Log", "Failed to download result..");
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			Log.e("Log", "Connect fail..");
			//Toast.makeText(Register.this,Log.e("Log", "Connect fail..") , Toast.LENGTH_LONG).show();
		} catch (IOException e) {
			e.printStackTrace();
			//Toast.makeText(Register.this,Log.e("Log", "Connect fail..") , Toast.LENGTH_LONG).show();
		}
		return str.toString();
}
  
 }
