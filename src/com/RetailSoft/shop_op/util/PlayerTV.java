package com.RetailSoft.shop_op.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

public class PlayerTV {
	private Context context;
	private String tvid;
	
	public PlayerTV(Context context,String url){
		
		this.context = context;
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(url));
		context.startActivity(i);	
	}		
}
