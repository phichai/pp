package com.RetailSoft.shop_op;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.RetailSoft.shop_op.util.JSONFeeder;
import com.RetailSoft.shop_op.util.OnJSONReceiver;
import com.RetailSoft.shop_op.util.Utils;

public class CrmTel extends ListActivity implements OnJSONReceiver {
	private static CrmTel mContext;
	private EfficientAdapter adapter;
	public static SharedPreferences prefs;
	private String mobileIP;
	private String mFlag;
	private Button mCustomerFlag;
	protected String mCallState;
	private Dialog callDialog;
	//private Dialog dialog;
	private Button btnCancel;
	protected String callState;
	private Button mCallBtn;
	protected String callType;
	private String shopBrand;
	private Utils mUtil;

	private static String shopName;
	public static String userID;
	
	private static List<HashMap<String, String>> mDataList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.call_list);
		mContext = this;
		mUtil = new Utils();
		prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
		shopName = prefs.getString("shopName", "");
		mobileIP = prefs.getString("mobileIP", "");
		userID = prefs.getString("userLogin", "");
		mCallState = prefs.getString("callState", "0");
		callType = prefs.getString("callType", "1");
		shopBrand = mUtil.getShopBrand(shopName);
		
		Log.i("shop Brand:",shopBrand);
		
		bindwidget();
		setListener();
		adapter = new EfficientAdapter(mContext);
		
		if(shopBrand.equalsIgnoreCase("OPS")){
			if(callType.equalsIgnoreCase("1")){
				mCallBtn.setText(R.string.op_callType1);
			}else{
				mCallBtn.setText(R.string.op_callType2);
			}
		}else{
			if(callType.equalsIgnoreCase("1")){
				mCallBtn.setText(R.string.callType1);
			}else{
				mCallBtn.setText(R.string.callType2);
			}
		}
		

		if(mCallState.equalsIgnoreCase("0")){
			mCustomerFlag.setText(R.string.callState1);
		}

		
	/*	
		callDialog = new Dialog(mContext);
		callDialog.setCancelable(false);
		callDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		callDialog.setContentView(R.layout.callstatus_dialog);
	/**/	


	}

	



	private void bindwidget() {
		// TODO Auto-generated method stub
		mCustomerFlag = (Button) findViewById(R.id.customerFlagBtn);
		mCallBtn = (Button) findViewById(R.id.callTypeBtn);

	}

	private void setListener() {
		// TODO Auto-generated method stub
		mCustomerFlag.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				showCustomDialog();
			}
		});
		
		mCallBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				showCallTypeDialog();
			}
		});

	}

	protected void showCallTypeDialog() {
		// TODO Auto-generated method stub
		final Dialog dialog = new Dialog(mContext);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.calltype);
		

		
		RadioButton mType1Btn = (RadioButton) dialog.findViewById(R.id.typeButton1);
		RadioButton mType2Btn = (RadioButton) dialog.findViewById(R.id.typeButton2);
	
		if(shopBrand.equalsIgnoreCase("OPS")){
			mType1Btn.setText(R.string.op_callType1);
			mType2Btn.setText(R.string.op_callType2);
		}
		
		mType1Btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				callType = "1";
				Editor edit = prefs.edit();
				edit.putString("callType", "1");
				edit.commit();
				if(shopBrand.equalsIgnoreCase("OPS")){
					mCallBtn.setText(R.string.op_callType1);
				}else{
					mCallBtn.setText(R.string.callType1);
				}
				accData();
				dialog.dismiss();
			}
		});
		
		mType2Btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				callType = "2";
				Editor edit = prefs.edit();
				edit.putString("callType", "2");
				edit.commit();
				if(shopBrand.equalsIgnoreCase("OPS")){
					mCallBtn.setText(R.string.op_callType2);
				}else{
					mCallBtn.setText(R.string.callType2);
				}
				accData();
				dialog.dismiss();
			}
		});
		
		
		dialog.show();
	}



	@Override
	protected void onResume() {
	    super.onResume();
	    accData();
	}
	
	private static class EfficientAdapter extends BaseAdapter {

		private LayoutInflater mInflater;
		private String pid;

		protected CallTask mCalltask;

		public EfficientAdapter(Context context) {
			mInflater = LayoutInflater.from(context);
		}

		@Override
		public int getCount() {
			int _row = (int) mDataList.size();
			return _row;

		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			final int pos = position;
			// TODO Auto-generated method stub
			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.testerrow, null);
				holder = new ViewHolder();
				holder.mName = (TextView) convertView
						.findViewById(R.id.memberName);
				holder.mTel = (TextView) convertView
						.findViewById(R.id.memberTel);
				holder.mCall = (TextView) convertView
						.findViewById(R.id.memberCallCount);
				holder.mTime = (TextView) convertView
						.findViewById(R.id.memberCallTime);
				holder.clayout = (LinearLayout) convertView
						.findViewById(R.id.testerlayout);
				/*
				 * holder.Mname = (TextView)
				 * convertView.findViewById(R.id.mname); holder.Cdata =
				 * (TextView) convertView .findViewById(R.id.calldata);
				 * holder.clayout = (LinearLayout) convertView
				 * .findViewById(R.id.clayout); holder.Timg = (ImageView)
				 * convertView .findViewById(R.id.imageView1); /*
				 */
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			final String phone_number = mDataList.get(pos).get(JSONFeeder.KEY_TEL);
			//final String phone_number = "0861643006";
			final String memberID = mDataList.get(pos)
					.get(JSONFeeder.KEY_MEMNO);
			final String memName = mDataList.get(pos).get(JSONFeeder.KEY_NAME);
			final String callYear = mDataList.get(pos).get(
					JSONFeeder.KEY_CALLID);
			final String memSurname = mDataList.get(pos).get(
					JSONFeeder.KEY_LNAME);
			
			final String callRound = mDataList.get(pos).get(
					JSONFeeder.KEY_CALLROUND);
						
			holder.mName.setText(memName + " " + memSurname);
			holder.mTel.setText(mDataList.get(pos).get(JSONFeeder.KEY_PROMO));
			
			
			holder.mCall.setText("โทรครั้งที่ #"+mDataList.get(pos).get(
					JSONFeeder.KEY_CALLCOUNT));
			holder.mTime.setText(mDataList.get(pos)
					.get(JSONFeeder.KEY_DATETEXT));

			holder.clayout.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					mCalltask = new CallTask();
					mCalltask.setVar(memName, memSurname, phone_number,
							memberID, callYear , callRound);
					mCalltask.execute();
				}
			});
			return convertView;
		}

		static class ViewHolder {
			TextView mName;
			TextView mTel;
			TextView mCall;
			TextView mTime;
			LinearLayout clayout;

		}

		class CallTask extends AsyncTask<String, Integer, Void> {

			private Dialog dialog;
			private ProgressBar progressBar;
			private Button btnCancel;
			private TextView tvLoading;
			private TextView tvPer;
			private HttpPost httpPost;
			private DefaultHttpClient client;
			private StringBuilder str;
			private JSONObject jsonObj;
			private String mName;
			private String mSname;
			private String mPhone;
			private String mMemID;
			private String mCallYear;
			private String mCallRound;
			
			
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
			
			}

			public void setVar(String memName, String memSurname,
					String phone_number, String memberID, String callYear,String callRound) {
				mName = memName;
				mSname = memSurname;
				mPhone = phone_number;
				mMemID = memberID;
				mCallYear = callYear;
				mCallRound = callRound;
			}

			@Override
			protected Void doInBackground(String... arg0) {
				// TODO Auto-generated method stub
				str = new StringBuilder();

				List<NameValuePair> prams = new ArrayList<NameValuePair>();
				prams.add(new BasicNameValuePair("phone", mPhone));
				prams.add(new BasicNameValuePair("memid", mMemID));
				prams.add(new BasicNameValuePair("memname", mName));
				prams.add(new BasicNameValuePair("memsname", mSname));
				prams.add(new BasicNameValuePair("callyear", mCallYear));
				prams.add(new BasicNameValuePair("calltype", "BDR"));
				prams.add(new BasicNameValuePair("callflag", "Start"));
				prams.add(new BasicNameValuePair("shopName", shopName));
				prams.add(new BasicNameValuePair("userID", userID));
				prams.add(new BasicNameValuePair("callround", mCallRound));
				
				String url = "http://mshop.ssup.co.th/shop_op/phone_calling.php";
				client = new DefaultHttpClient();
				httpPost = new HttpPost(url);
				try {
					httpPost.setHeader("Content-Type",
							"application/x-www-form-urlencoded;charset=UTF-8");
					httpPost.setEntity(new UrlEncodedFormEntity(prams, "UTF-8"));

					// Add your data

					HttpResponse response = client.execute(httpPost);
					StatusLine statusLine = response.getStatusLine();
					int statusCode = statusLine.getStatusCode();
					if (statusCode == 200) { // Status OK
						HttpEntity entity = response.getEntity();
						InputStream content = entity.getContent();
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(content));
						String line;
						while ((line = reader.readLine()) != null) {
							str.append(line);
						}
					} else {
						Log.e("Log", "Failed to connect " + url
								+ " AND update ");
					}
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				super.onPostExecute(result);
				//mContext.startActivity(new Intent(mContext,CallPrepare.class));
				
			
				//dialog.dismiss();
				try {
					jsonObj = new JSONObject(str.toString());
					String cid = jsonObj.getString("transID");
					Editor edit = prefs.edit();
					edit.putString("calltransID", cid);
					edit.commit();
					Intent myIntent = new Intent(Intent.ACTION_DIAL,
							Uri.parse("tel:" + mPhone));
					mContext.startActivityForResult(myIntent, 0);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				/**/
				/*
				Intent mIntent = new Intent(mContext,CallPrepare.class);
				mIntent.putExtra("phone", mPhone);
				mIntent.putExtra("memid", mMemID);
				mIntent.putExtra("memname", mName);
				mIntent.putExtra("memsname", mSname);
				mIntent.putExtra("callyear", mCallYear);
				mIntent.putExtra("calltype", "BDR");
				mIntent.putExtra("callflag", "Start");
				mIntent.putExtra("shopName", shopName);
				mIntent.putExtra("userID", userID);
				mContext.startActivity(mIntent);
				/**/
				
			}
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		Log.i("CALLIN", "call return");
		mContext.startActivity(new Intent(mContext, CallPrepare.class));
		/*
		final Dialog dialog = new Dialog(mContext);
		dialog.setCancelable(false);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.callstatus_dialog);
		
		RadioButton mCallState2 = (RadioButton) dialog.findViewById(R.id.callState2);
		RadioButton mCallState3 = (RadioButton) dialog.findViewById(R.id.callState3);
		RadioButton mCallState4 = (RadioButton) dialog.findViewById(R.id.callState5);
		RadioButton mCallState5 = (RadioButton) dialog.findViewById(R.id.callState5);
		RadioButton mCallState6 = (RadioButton) dialog.findViewById(R.id.callState6);
		EditText mCallDesc = (EditText) dialog.findViewById(R.id.tvper);
		Button mSaveBtn = (Button) dialog.findViewById(R.id.saveBtn);

		mCallState2.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				// TODO Auto-generated method stub
				callState = "2";
			}
		});
		mCallState3.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				// TODO Auto-generated method stub
				callState = "3";
			}
		});
		mCallState4.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				// TODO Auto-generated method stub
				callState = "4";
			}
		});
		mCallState5.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				// TODO Auto-generated method stub
				callState = "5";
			}
		});
		mCallState6.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				// TODO Auto-generated method stub
				callState = "6";
			}
		});
		
		mSaveBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//dialog.dismiss();
				dialog.hide();
				//finish();
			}
		});		
		
		dialog.show();
		
		/**/
		//callDialog.show();
		
	}

	@Override
	public void onReceiveData(ArrayList<HashMap<String, String>> result) {
		// TODO Auto-generated method stub
		mDataList = result;
		setListAdapter(adapter);

	}

	protected void showCustomDialog() {
		// TODO Auto-generated method stub
		final Dialog dialog = new Dialog(mContext);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		
		if(shopBrand.equalsIgnoreCase("OPS")){
			dialog.setContentView(R.layout.op_calltype);
			RadioGroup mStateRGroup = (RadioGroup) dialog.findViewById(R.id.cStateRGroup);
			mStateRGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(RadioGroup group, int checkedId) {
					// TODO Auto-generated method stub

					switch (checkedId) {
					case R.id.radioButton1:
						mCallState = "0";
						mCustomerFlag.setText(R.string.callState1);
						break;
					case R.id.radioButton2:
						mCallState = "2";
						mCustomerFlag.setText(R.string.op_callState2);
						break;
					case R.id.radioButton3:
						mCallState = "3";
						mCustomerFlag.setText(R.string.op_callState3);
						break;
					case R.id.radioButton4:
						mCallState = "4";
						mCustomerFlag.setText(R.string.op_callState4);
						break;
					case R.id.radioButton5:
						mCallState = "5";
						mCustomerFlag.setText(R.string.op_callState5);
						break;
					default:
						break;
					}
					Log.i("Calling Type",mCallState);
					Editor editor = prefs.edit();
					editor.putString("shopName", mCallState);
					editor.commit(); 
					accData();
					dialog.dismiss();
					//Log.i("chk value",String.valueOf(checkedId));
					
				}
			});			
		}else{
			dialog.setContentView(R.layout.operationtype);
			RadioGroup mStateRGroup = (RadioGroup) dialog.findViewById(R.id.cStateRGroup);
			mStateRGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(RadioGroup group, int checkedId) {
					// TODO Auto-generated method stub

					switch (checkedId) {
					case R.id.radioButton2:
						mCallState = "2";
						mCustomerFlag.setText(R.string.callState2);

						break;
					case R.id.radioButton3:
						mCallState = "3";
						mCustomerFlag.setText(R.string.callState3);
						break;
					case R.id.radioButton4:
						mCallState = "4";
						mCustomerFlag.setText(R.string.callState4);
						break;
					case R.id.radioButton5:
						mCallState = "5";
						mCustomerFlag.setText(R.string.callState5);
						break;
					case R.id.radioButton6:
						mCallState = "6";
						mCustomerFlag.setText(R.string.callState6);
						break;
					case R.id.radioButton7:
						mCallState = "7";
						mCustomerFlag.setText(R.string.callState7);
						break;
					case R.id.radioButton8:
						mCallState = "8";
						mCustomerFlag.setText(R.string.callState8);
						break;
					case R.id.radioButton9:
						mCallState = "9";
						mCustomerFlag.setText(R.string.callState9);
						break;
					case R.id.radioButton10:
						mCallState = "10";
						mCustomerFlag.setText(R.string.callState10);
						break;
					case R.id.radioButton1:
						mCallState = "0";
						mCustomerFlag.setText(R.string.callState1);
						break;
					default:
						break;
					}
					//Log.i("Calling Type",mCallState);
					Editor editor = prefs.edit();
					editor.putString("shopName", mCallState);
					editor.commit(); 
					accData();
					dialog.dismiss();
					//Log.i("chk value",String.valueOf(checkedId));
					
				}
			});
			
		}

		dialog.show();
	}
	
	public void accData() {
		Log.i("get data","get data");
		JSONFeeder.setQstr(shopName, mCallState,callType);
		JSONFeeder.getData(mContext);
	}
}
