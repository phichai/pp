package com.RetailSoft.shop_op;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.RetailSoft.shop_op.util.BitmapUtil;
import com.RetailSoft.shop_op.util.CMAssetBundle;
import com.RetailSoft.shop_op.util.NetworkUtil;
import com.RetailSoft.shop_op.util.PackageInfoManager;

public class MainActivity extends Activity {

	private Button mCrmTelBtn;
	private Button mShopProbBtn;
	private MainActivity mContext;
	private Button mMobilecamBtn;
	private SharedPreferences prefs;
	private String defBg;
	private Bitmap bm;
	private ImageView mVVbg;
	private BitmapUtil mBmUtil;
	public String deviceIP;
	public String qrStr;
	private NetworkUtil nwUtil;
	public String ipCamStatus;
	private WCSTask mWebcamTask;
	public String appStatus;
	protected WCSTaskAPP mWebcamAPPTask;
	private Button mOperListBtn;
	private Button mOpenBoxControlBtn;
	PackageInfoManager pim;
	private Button mStreamingBtn;
	private TextView mAppNameShow;
	private String defShopName;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mContext = this;
		pim = new PackageInfoManager(mContext);
		
		//get Notification
//		Intent intent = new Intent(mContext,NatiReceiver.class);
//	    startService(intent);
		
		
		bindwidget();
		setListener();

		mBmUtil = new BitmapUtil();

		prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
		defBg = prefs.getString("defWall", "");
		nwUtil = new NetworkUtil();
		defShopName = prefs.getString("shopName", "");
		mAppNameShow.setText(getResources().getString(R.string.app_name)+" : "+defShopName);

		deviceIP = chkWIFIConnect();
		if (defBg.equalsIgnoreCase("")) {

		} else {
			bm = mBmUtil.StringToBitMap(defBg);
			mVVbg.setImageBitmap(bm);
		}

		CMAssetBundle.makedir(ShopGlobalVariable.IMG_PATH);
		ipCamStatus = "0";
		appStatus = "1";
		mWebcamTask = new WCSTask();
		mWebcamTask.execute();
	}

	private String chkWIFIConnect() {
		// TODO Auto-generated method stub
		String tmp = NetworkUtil.getIPAddress(true);
		return tmp;
	}

	private void bindwidget() {
		// TODO Auto-generated method stub
		mCrmTelBtn = (Button) findViewById(R.id.crmTelBtn);
		mMobilecamBtn = (Button) findViewById(R.id.mobileCam);
		mOperListBtn = (Button) findViewById(R.id.operBtn);
		mOpenBoxControlBtn = (Button) findViewById(R.id.openBoxControl);
		mStreamingBtn	= (Button) findViewById(R.id.ssupstream);
		mVVbg = (ImageView) findViewById(R.id.vvbg);
		mAppNameShow = (TextView) findViewById(R.id.appnameshow);
	}

	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		ipCamStatus = "0";
		appStatus = "1";
		mWebcamTask = new WCSTask();
		mWebcamTask.execute();
	}

	private void setListener() {
		// TODO Auto-generated method stub

		mCrmTelBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent myIntent = new Intent(mContext, CrmTel.class);
				startActivity(myIntent);
			}
		});
		/**/
		/*
		 * mShopProbBtn.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub Intent myIntent = new Intent(mContext, ShopProblem.class);
		 * startActivity(myIntent); } }); /*
		 */
		mMobilecamBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ipCamStatus = "1";
				appStatus = "1";
				mWebcamAPPTask = new WCSTaskAPP();
				mWebcamAPPTask.execute();

			}
		});

		mOperListBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				startActivity(new Intent(mContext, OperationChecklist.class));
			}
		});

		mOpenBoxControlBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				startActivity(pim.openBoxControl());
			}
		});
		
		mStreamingBtn.setOnClickListener(new OnClickListener() {
			//String url = "http://112.121.152.34:80/stream/5195e80fe1ed0/playlist.m3u8";
			@Override
			public void onClick(View v) {
				//Intent intent = new Intent(mContext,VideoViewDemo.class);
				Intent intent = new Intent(mContext,playerstream.class);
				//intent.putExtra("urlstream",url);
				intent.putExtra("shopname", defShopName);
				startActivity(intent);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	class WCSTask extends AsyncTask<String, Integer, Void> {

		private StringBuilder str;
		private DefaultHttpClient client;
		private HttpPost httpPost;
		private JSONObject jsonObj;

		@Override
		protected Void doInBackground(String... arg0) {
			// TODO Auto-generated method stub
			str = new StringBuilder();
			List<NameValuePair> prams = new ArrayList<NameValuePair>();
			prams.add(new BasicNameValuePair("uid", qrStr));
			prams.add(new BasicNameValuePair("mobileIP", deviceIP));
			prams.add(new BasicNameValuePair("wcamStatus", ipCamStatus));
			prams.add(new BasicNameValuePair("appStatus", appStatus));

			String url = "http://mshop.ssup.co.th/shop_op/wcstatus.php";
			client = new DefaultHttpClient();
			httpPost = new HttpPost(url);
			try {
				httpPost.setEntity(new UrlEncodedFormEntity(prams));
				HttpResponse response = client.execute(httpPost);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) { // Status OK
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						str.append(line);
					}
				} else {
					Log.e("Log", "Failed to connect " + url + " AND update ");
				}
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Log.i("return", str.toString());
		}
	}

	class WCSTaskAPP extends AsyncTask<String, Integer, Void> {

		private StringBuilder str;
		private DefaultHttpClient client;
		private HttpPost httpPost;
		private JSONObject jsonObj;

		@Override
		protected Void doInBackground(String... arg0) {
			// TODO Auto-generated method stub
			str = new StringBuilder();
			List<NameValuePair> prams = new ArrayList<NameValuePair>();
			prams.add(new BasicNameValuePair("uid", qrStr));
			prams.add(new BasicNameValuePair("mobileIP", deviceIP));
			prams.add(new BasicNameValuePair("wcamStatus", ipCamStatus));
			prams.add(new BasicNameValuePair("appStatus", appStatus));

			String url = "http://mshop.ssup.co.th/shop_op/wcstatus.php";
			client = new DefaultHttpClient();
			httpPost = new HttpPost(url);
			try {
				httpPost.setEntity(new UrlEncodedFormEntity(prams));
				HttpResponse response = client.execute(httpPost);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) { // Status OK
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						str.append(line);
					}
				} else {
					Log.e("Log", "Failed to connect server..");
				}
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			Log.i("return", str.toString());
			Intent LaunchIntent = getPackageManager()
					.getLaunchIntentForPackage("com.pas.webcam");
			startActivityForResult(LaunchIntent, 0);
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			onBackPressed();
		}
		return true;
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		ipCamStatus = "0";
		appStatus = "0";
		mWebcamTask = new WCSTask();
		mWebcamTask.execute();
		// finish();

	}
}
