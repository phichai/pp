package com.RetailSoft.shop_op;

import android.app.Dialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.RetailSoft.shop_op.util.BitmapUtil;
import com.RetailSoft.shop_op.util.JSONChklistFeeder;
import com.RetailSoft.shop_op.util.OnJSONReceiver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OperationChecklist extends ListActivity implements OnJSONReceiver {

	private EfficientAdapter adapter;
	private static OperationChecklist mContext;
	private BitmapUtil mBmUtil;
	private SharedPreferences prefs;
	private String shopName;
	private String userID;
	private String brand;
	private String mOperationFeq;
	private String mOperationType;
	private Button mOpChklistBtn;
	private Button mChktypeBtn;
	private static List<HashMap<String, String>> mDataList;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.operationchklist);
		mContext = this;

		binwidget();
		setListener();

		prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
		shopName = prefs.getString("shopName", "");
		userID = prefs.getString("userid", "");
		mOperationFeq = prefs.getString("defFOPT", "D");
		mOperationType = prefs.getString("defTOPT", "M");
		/*
		 * String shopPrefix = shopNO.substring(0, 1);
		 * 
		 * if (shopPrefix.equalsIgnoreCase("7")) { brand = "OP"; } else if
		 * (shopPrefix.equalsIgnoreCase("6")) { brand = "CPS"; } else if
		 * (shopPrefix.equalsIgnoreCase("5")) { brand = "GNC"; }/*
		 */
		adapter = new EfficientAdapter(this);
		
		displayTask(shopName, mOperationFeq, mOperationType);

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Log.i("resume",shopName+":"+mOperationFeq+":"+mOperationType);
		displayTask(shopName, mOperationFeq, mOperationType);
	}

	private void displayTask(String shopid2, String feq, String type) {
		// TODO Auto-generated method stub
		// Log.i("data", feq + " :: " + type + "::"+shopName);
		defaultButton(feq, type);
		JSONChklistFeeder.setQstr(shopid2, feq, type);
		JSONChklistFeeder.getData(mContext);
	}

	private void defaultButton(String feq, String type) {
		// TODO Auto-generated method stub
		if (feq == "M") {
			mOpChklistBtn.setText(R.string.monthly);
		} else if (feq == "W") {
			mOpChklistBtn.setText(R.string.weekly);
		} else {
			mOpChklistBtn.setText(R.string.daily);
			mOperationFeq = "D";
		}

		if (type == "E") {
			mChktypeBtn.setText("Extend");
		} else {
			mChktypeBtn.setText("Main");
			mOperationType = "M";
		}

	}

	private void binwidget() {
		// TODO Auto-generated method stub
		mOpChklistBtn = (Button) findViewById(R.id.chklistTypeBtn);
		mChktypeBtn = (Button) findViewById(R.id.optTypeBtn);

	}

	private void setListener() {
		// TODO Auto-generated method stub
		mOpChklistBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showCustomDialog();
			}
		});


		mChktypeBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				showChktypDialog();
			}
		});

	}

	protected void showChktypDialog() {
		// TODO Auto-generated method stub
		final Dialog tdislog = new Dialog(mContext);
		tdislog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		tdislog.setContentView(R.layout.checklisttype);

		RadioButton r1btn = (RadioButton) tdislog
				.findViewById(R.id.radioButton1);

		r1btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setChklistType("M");
				displayTask(shopName, mOperationFeq, "M");
				tdislog.hide();
			}
		});

		RadioButton r2btn = (RadioButton) tdislog
				.findViewById(R.id.radioButton2);
		r2btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setChklistType("E");
				displayTask(shopName, mOperationFeq, "E");
				tdislog.hide();
			}
		});

		RadioButton r3btn = (RadioButton) tdislog
				.findViewById(R.id.radioButton3);
		r3btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setChklistType("C");
				displayTask(shopName, mOperationFeq, "C");
				tdislog.hide();
			}
		});

		if (mOperationType == "C") {
			r3btn.setChecked(true);
		} else if (mOperationType == "E") {
			r2btn.setChecked(true);
		} else {
			r1btn.setChecked(true);
		}

		tdislog.show();
	}

	protected void showCustomDialog() {
		// TODO Auto-generated method stub
		final Dialog dialog = new Dialog(mContext);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.chklistfeq);

		RadioButton r1btn = (RadioButton) dialog
				.findViewById(R.id.radioButton1);

		r1btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setChklistFeq("D");
				displayTask(shopName, "D", mOperationType);
				dialog.hide();
			}
		});

		RadioButton r2btn = (RadioButton) dialog
				.findViewById(R.id.radioButton2);
		r2btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setChklistFeq("W");
				displayTask(shopName, "W", mOperationType);
				dialog.hide();
			}
		});

		RadioButton r3btn = (RadioButton) dialog
				.findViewById(R.id.radioButton3);
		r3btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setChklistFeq("M");
				displayTask(shopName, "M", mOperationType);
				dialog.hide();
			}
		});

		if (mOperationFeq == "D") {
			r1btn.setChecked(true);
		} else if (mOperationFeq == "W") {
			r2btn.setChecked(true);
		} else {
			r3btn.setChecked(true);
		}

		dialog.show();
	}

	protected void setChklistFeq(String c) {
		// TODO Auto-generated method stub
		mOperationFeq = c;
		Editor editor = prefs.edit();
		editor.putString("defFOPT", c);
		editor.commit();
		defaultButton(mOperationType, c);

	}

	protected void setChklistType(String c) {
		// TODO Auto-generated method stub
		mOperationType = c;
		Editor editor = prefs.edit();
		editor.putString("defTOPT", c);
		editor.commit();
		defaultButton(c, mOperationFeq);
	}

	private static class EfficientAdapter extends BaseAdapter {

		private LayoutInflater mInflater;

		public EfficientAdapter(Context context) {
			mInflater = LayoutInflater.from(context);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			int _row = (int) mDataList.size();
			return _row;

		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;

			final int pos = position;
			Log.i("POS Value",String.valueOf(pos));

			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.stdrow2, null);
				holder = new ViewHolder();
				holder.clayout = (LinearLayout) convertView
						.findViewById(R.id.clayout);
				holder.iButton = (Button) convertView.findViewById(R.id.appBtn);
				holder.iStatusLayout = (LinearLayout) convertView
						.findViewById(R.id.statusLayout);
				holder.rosStatuslayout = (LinearLayout) convertView
						.findViewById(R.id.statusROS);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			
				String chktask = mDataList.get(pos).get(JSONChklistFeeder.KEY_TRANS);
				String chkpass = mDataList.get(pos).get(JSONChklistFeeder.KEY_PASS);
				String rosApprove = mDataList.get(pos).get(JSONChklistFeeder.KEY_ROSPASS);
				String rosComment = mDataList.get(pos).get(JSONChklistFeeder.KEY_ROSCOMMENT);
				String keyName = mDataList.get(pos).get(JSONChklistFeeder.KEY_NAME);
				Log.i("ros_data",rosApprove+"::"+rosComment);
				
				if (chktask.equalsIgnoreCase("null")) {
					// LayoutParams params = new
					// LayoutParams(0,LayoutParams.MATCH_PARENT);
					holder.iButton.setTextAppearance(mContext,
							R.style.btnStyleBeige);
					holder.iStatusLayout.setVisibility(View.GONE);
					holder.rosStatuslayout.setVisibility(View.GONE);
					/**/
				} else {
					/*						
					if (rosApprove.equalsIgnoreCase("2")) {
						//holder.clayout.setVisibility(View.GONE);
						//holder.clayout.removeAllViews();
						
					} else if (rosApprove.equalsIgnoreCase("1")) {
						holder.rosStatuslayout.setBackgroundColor(Color.RED);
					} else {
						holder.rosStatuslayout.setVisibility(View.GONE);
					}
					
					if (chkpass.equalsIgnoreCase("2")) {
						Log.i("case 2", String.valueOf(R.color.green));
						holder.iStatusLayout.setBackgroundColor(Color.GREEN);
					} else if (chkpass.equalsIgnoreCase("1")) {
						Log.i(" case 1", String.valueOf(R.color.red));
						holder.iStatusLayout.setBackgroundColor(Color.RED);
					} else {
						Log.i(" case oth", String.valueOf(R.color.gray));
						holder.iStatusLayout.setBackgroundColor(Color.GRAY);
					}
					
					holder.iButton.setTextAppearance(mContext,
							R.style.btnStyleBeigeDisabled);
					holder.iStatusLayout.setVisibility(View.VISIBLE);
					/**/
				}
				
				Log.i("Key NAme",keyName);
				holder.iButton.setText(mDataList.get(pos).get(
						JSONChklistFeeder.KEY_NAME));
				holder.iButton.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent mIntent = null;
						if (mDataList.get(pos).get(JSONChklistFeeder.KEY_ID)
								.equalsIgnoreCase("0")) {
							mIntent = new Intent(mContext,
									CustomTaskDetail.class);
						} else {
							mIntent = new Intent(mContext, TaskDetail.class);

						}

						mIntent.putExtra("taskname",
								mDataList.get(pos).get(JSONChklistFeeder.KEY_NAME));
						mIntent.putExtra("masterid",
								mDataList.get(pos).get(JSONChklistFeeder.KEY_ID));
						mIntent.putExtra("tasktrans",
								mDataList.get(pos).get(JSONChklistFeeder.KEY_TRANS));
						mContext.startActivity(mIntent);
						mContext.finish();

					}
				});

			

			/**/
			return convertView;

		}

		static class ViewHolder {
			Button iButton;
			LinearLayout iStatusLayout;
			LinearLayout rosStatuslayout;
			LinearLayout clayout;
		}

	}

	@Override
	public void onReceiveData(ArrayList<HashMap<String, String>> result) {
		// TODO Auto-generated method stub
		mDataList = result;
		setListAdapter(adapter);

	}

}
