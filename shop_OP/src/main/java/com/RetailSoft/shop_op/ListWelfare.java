package com.RetailSoft.shop_op;

import android.app.ListActivity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.RetailSoft.shop_op.util.JSONWelfareFeeder;
import com.RetailSoft.shop_op.util.NetworkUtil;
import com.RetailSoft.shop_op.util.OnJSONReceiver;
import com.RetailSoft.shop_op.util.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Created by IS-PATTHANAPONG on 04/04/2559.
 */
public class ListWelfare extends ListActivity implements OnJSONReceiver {


    private static ListWelfare mContext;


    private static List<HashMap<String, String>> mDataList;
    private EfficientAdapter adapter;
    private Utils mUtil;

    private String jsdata;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.videolist);

        mContext = this;
        mUtil = new Utils();
        Log.i("Post","OK");
        Bundle extras = getIntent().getExtras();
        if(extras == null) {
            jsdata= null;
        } else {
            jsdata= extras.getString("data");

        }
        Log.i("Post Data", jsdata);
        adapter = new EfficientAdapter(this);
        //generateData();

        displayTask();
    }

    public String chkWIFIConnect() {
        // TODO Auto-generated method stub
        String tmp = NetworkUtil.getIPAddress(true);
        return tmp;
    }
    @Override
    public void onReceiveData(ArrayList<HashMap<String, String>> result) {
        mDataList = result;
        setListAdapter(adapter);
    }


      private static class EfficientAdapter extends BaseAdapter {

        private LayoutInflater mInflater;


          public EfficientAdapter(Context context) {
            mInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            int _row = (int) mDataList.size();
            return _row;

        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;

            final int pos = position;

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.stdrow3, null);
                holder = new ViewHolder();
                holder.iamount = (TextView) convertView.findViewById(R.id.amount);
                holder.ibecause = (TextView) convertView.findViewById(R.id.because);
                holder.iwhere = (TextView) convertView.findViewById(R.id.where);
                holder.idate = (TextView) convertView.findViewById(R.id.appdate);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.iwhere.setText("สถานพยาบาล : "+mDataList.get(pos).get(JSONWelfareFeeder.KEY_WHERE));
            holder.ibecause.setText(mDataList.get(pos).get(JSONWelfareFeeder.KEY_BECAUSE));
            holder.iamount.setText("จำนวนเงินที่อนุมัติ : " + mDataList.get(pos).get(JSONWelfareFeeder.KEY_CFAMOUNT)+"(จาก"+mDataList.get(pos).get(JSONWelfareFeeder.KEY_AMOUNT)+")");
            String appDate = mDataList.get(pos).get(JSONWelfareFeeder.KEY_DATE);
            if(appDate.startsWith("0000-")){
                holder.idate.setText("รอการอนุมัติ");
            }else{
                if(mDataList.get(pos).get(JSONWelfareFeeder.KEY_USTATUS).equalsIgnoreCase("0")){
                    holder.idate.setText("ไม่อนุมัติ : " + appDate);
                }else{
                    holder.idate.setText("วันที่อนุมัติ : " + appDate);
                }
            }

			/**/
            return convertView;

        }

        static class ViewHolder {
            TextView iwhere;
            TextView iamount;
            TextView ibecause;
            TextView idate;
        }

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        displayTask();
    }

    private void displayTask() {
        // TODO Auto-generated method stub
        // Log.i("data", feq + " :: " + type + "::"+shopName);
        JSONWelfareFeeder.setData(jsdata);
        JSONWelfareFeeder.getData(mContext);
    }


}
