package com.RetailSoft.shop_op;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;

import com.db.chart.Tools;
import com.db.chart.model.Bar;
import com.db.chart.model.BarSet;
import com.db.chart.view.BarChartView;
import com.db.chart.view.XController;
import com.db.chart.view.YController;
import com.db.chart.view.animation.Animation;

/**
 * Created by IS-PATTHANAPONG on 04/04/2559.
 */
public class BDReport extends Activity {
    private BDReport mContext;
    private BarChartView mChart;
    private final String[] mLabels= {"Target","BD Archive"};
    private final float [][] mValues = {{26.5f,15f}, {7.5f,3f}};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bdchart);

        mContext = this;
        mChart = (BarChartView) findViewById(R.id.chart1);

        // Data
        /*
        BarSet barSet = new BarSet(mLabels, mValues[0]);
        barSet.setColor(Color.parseColor("#fc2a53"));
        mChart.addData(barSet);
        /**/
        BarSet barSet = new BarSet();
        Bar bar;
        for(int i = 0; i < mLabels.length; i++){
            bar = new Bar(mLabels[i], mValues[0][i]);
            switch (i){
                case 0: bar.setColor(Color.parseColor("#77c63d")); break;
                case 1: bar.setColor(Color.parseColor("#27ae60")); break;
                case 2: bar.setColor(Color.parseColor("#47bac1")); break;
                case 3: bar.setColor(Color.parseColor("#16a085")); break;
                case 4: bar.setColor(Color.parseColor("#3498db")); break;
                default: break;
            }
            barSet.addBar(bar);
        }
        mChart.setBarSpacing(Tools.fromDpToPx(40));
        mChart.setRoundCorners(Tools.fromDpToPx(2));
        mChart.setBarBackgroundColor(Color.parseColor("#592932"));

        // Chart
        mChart.setXAxis(false)
                .setYAxis(false)
                .setXLabels(XController.LabelPosition.OUTSIDE)
                .setYLabels(YController.LabelPosition.NONE)
                .setLabelsColor(Color.parseColor("#86705c"))
                .setAxisColor(Color.parseColor("#86705c"));



        int[] order = {0,1};
        mChart.show(new Animation().setOverlap(0f,order)
                );
    }

}
