package com.RetailSoft.shop_op;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.RadioButton;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class MemberInfo extends Activity {

	private String phone;
	private MemberInfo mContext;
	private SharedPreferences prefs;
	private String userID;
	private String shopName;
	private String mPhone;
	private String mMemID;
	private String mName;
	private String mSname;
	private String mCallYear;
	private RadioButton mCallState2;
	private RadioButton mCallState3;
	private RadioButton mCallState4;
	private RadioButton mCallState5;
	private RadioButton mCallState6;
	private RadioButton mCallState7;
	private RadioButton mCallState8;
	private RadioButton mCallState9;
	private RadioButton mCallState10;
	protected EditText mCallDesc;
	private Button mSaveBtn;
	protected String callState;
	private String callID;
	protected CallProcessTask mCallProcess;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.callstatus_dialog);

		mContext = this;
		bindwidget();
		setListener();

		Intent mIntent = getIntent();
		prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
		callID = prefs.getString("calltransID", "");
		mPhone = mIntent.getStringExtra("phone");
		mMemID = mIntent.getStringExtra("memid");
		mName = mIntent.getStringExtra("memname");
		mSname = mIntent.getStringExtra("memsname");
		mCallYear = mIntent.getStringExtra("callyear");
		shopName = mIntent.getStringExtra("shopName");
		userID = mIntent.getStringExtra("userID");
		/*
		 * Intent myIntent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" +
		 * mPhone)); startActivityForResult(mIntent, 0); /*
		 */
	}

	private void bindwidget() {
		// TODO Auto-generated method stub
		mCallState2 = (RadioButton) findViewById(R.id.callState2);
		mCallState3 = (RadioButton) findViewById(R.id.callState3);
		mCallState4 = (RadioButton) findViewById(R.id.callState4);
		mCallState5 = (RadioButton) findViewById(R.id.callState5);
		mCallState6 = (RadioButton) findViewById(R.id.callState6);
		mCallState7 = (RadioButton) findViewById(R.id.callState7);
		mCallState8 = (RadioButton) findViewById(R.id.callState8);
		mCallState9 = (RadioButton) findViewById(R.id.callState9);
		mCallState10 = (RadioButton) findViewById(R.id.callState10);
		mCallDesc = (EditText) findViewById(R.id.tvper);
		mSaveBtn = (Button) findViewById(R.id.saveBtn);

	}

	private void setListener() {
		// TODO Auto-generated method stub
		mCallState2.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				// TODO Auto-generated method stub
				callState = "2";
			}
		});
		mCallState3.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				// TODO Auto-generated method stub
				callState = "3";
			}
		});
		mCallState4.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				// TODO Auto-generated method stub
				callState = "4";
			}
		});
		mCallState5.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				// TODO Auto-generated method stub
				callState = "5";
			}
		});
		mCallState6.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				// TODO Auto-generated method stub
				callState = "6";
			}
		});

		mCallState6.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				// TODO Auto-generated method stub
				callState = "6";
			}
		});

		mCallState7.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				// TODO Auto-generated method stub
				callState = "7";
			}
		});
		mCallState8.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				// TODO Auto-generated method stub
				callState = "8";
			}
		});
		mCallState9.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				// TODO Auto-generated method stub
				callState = "9";
			}
		});
		mCallState10.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				// TODO Auto-generated method stub
				callState = "10";
			}
		});

		mSaveBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mCallProcess = new CallProcessTask();
				mCallProcess.execute();
				
			}
		});

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
	}

	/*
	 * @Override public void onActivityResult(int requestCode, int resultCode,
	 * Intent intent) { //Log.i("result",String.valueOf(resultCode)); super
	 * Log.i("result Intetnt",String.valueOf(intent.getScheme()));
	 * 
	 * finish(); }
	 * 
	 * /*
	 */
	class CallProcessTask extends AsyncTask<String, Integer, Void> {

		private DefaultHttpClient client;
		private HttpPost httpPost;
		private StringBuilder str;
		private Dialog dialog;
		private Button dCancel;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new Dialog(mContext);
			dialog.setCancelable(false);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.loadingdialog);
			
			dCancel = (Button) dialog.findViewById(R.id.btncancel);
			dCancel.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					mCallProcess.cancel(true);
					dialog.dismiss();
				}
			});
			
			dialog.show();
		}

		@Override
		protected Void doInBackground(String... arg0) {
			// TODO Auto-generated method stub
			Log.i("Callprepare","Intask");
			str = new StringBuilder();
			List<NameValuePair> prams = new ArrayList<NameValuePair>();
			prams.add(new BasicNameValuePair("cid", callID));
			prams.add(new BasicNameValuePair("callFlag",callState));
			prams.add(new BasicNameValuePair("userID",userID));
			prams.add(new BasicNameValuePair("callDesc", mCallDesc.getText().toString()));

			String url = "http://mshop.ssup.co.th/shop_op/phone_calling_desc.php";
			client = new DefaultHttpClient();
			httpPost = new HttpPost(url);
			try {
				httpPost.setHeader("Content-Type","application/x-www-form-urlencoded;charset=UTF-8");
				httpPost.setEntity(new UrlEncodedFormEntity(prams, "UTF-8"));
				
				// Add your data

				HttpResponse response = client.execute(httpPost);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				
				if (statusCode == 200) { // Status OK
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						str.append(line);
					}
					Log.i("return",str.toString());
					/*
					 * HttpEntity entity = response.getEntity(); InputStream
					 * content = entity.getContent(); BufferedReader reader =
					 * new BufferedReader( new InputStreamReader(content));
					 * String line; while ((line = reader.readLine()) != null) {
					 * str.append(line); } /*
					 */
				} else {
					Log.e("Log", "Failed to connect " + url + " AND update ");
				}
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			//Log.i("return",str.toString());
			dialog.dismiss();
			finish();
		}
	}

}
