 package com.RetailSoft.shop_op;

 import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.VideoView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

 public class DownloadVideo extends Activity
 {
  private String shopname="OP";
  private SharedPreferences prefs;
  private String user_id;
  private static ProgressDialog progressDialog;
  private Context mContext;
  String videourl;  
  VideoView videoView ;
  private String filename;
  private MregTask mTsk;
  private String urldata;


     @SuppressLint("NewApi")
protected void onCreate(Bundle savedInstanceState)
  {

   super.onCreate(savedInstanceState);
   requestWindowFeature(Window.FEATURE_NO_TITLE);
   getWindow().setFlags(
   WindowManager.LayoutParams.FLAG_FULLSCREEN,
   WindowManager.LayoutParams.FLAG_FULLSCREEN);
   setContentView(R.layout.play_video);
   mContext  = this;

   // Permission StrictMode
   if (android.os.Build.VERSION.SDK_INT > 9) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
   }
   
  


/*
    progressDialog = ProgressDialog.show(DownloadVideo.this, "", "Downloading video...", true);
    progressDialog.setCancelable(true);  
   /**/
    Bundle extras = getIntent().getExtras();
	   if(extras != null){
           filename = extras.getString("filename");
           urldata = extras.getString("urlpost");
	   }
   Log.i("URL",filename);

    mTsk = new MregTask();
    mTsk.setfile(filename);
    mTsk.execute();

     

    }

     class MregTask extends AsyncTask<String, Integer, Void> {
         private String myfile;
         private Dialog dialog;
         private ProgressBar progressBar;
         private TextView tvLoading;
         private TextView tvPer;
         private Button btnCancel;
         private StringBuilder str;
         private DefaultHttpClient client;
         private HttpPost httpPost;
         private JSONObject jsonObj;

         @Override
         protected Void doInBackground(String... arg0) {
             // TODO Auto-generated method stub
             str = new StringBuilder();
             List<NameValuePair> prams = new ArrayList<NameValuePair>();
             prams.add(new BasicNameValuePair("file", filename));




             client = new DefaultHttpClient();
             Log.i("URL API",urldata);
             Log.i("URL FILE",filename);
             httpPost = new HttpPost(urldata);
             try {
                 httpPost.setEntity(new UrlEncodedFormEntity(prams));
                 HttpResponse response = client.execute(httpPost);
                 prams.add(new BasicNameValuePair("file", filename));

                 StatusLine statusLine = response.getStatusLine();
                 int statusCode = statusLine.getStatusCode();
                 if (statusCode == 200) { // Status OK
                     HttpEntity entity = response.getEntity();
                     InputStream content = entity.getContent();
                     BufferedReader reader = new BufferedReader(
                             new InputStreamReader(content));
                     String line;
                     while ((line = reader.readLine()) != null) {
                         str.append(line);
                     }
                 } else {
                     Log.e("Log", "Failed to connect server..");
                 }
             } catch (ClientProtocolException e) {
                 e.printStackTrace();
             } catch (IOException e) {
                 e.printStackTrace();
             }
             Log.i("URL --= aaaa =--", str.toString());
             // TODO Auto-generated method stub
             return null;
         }

         @Override
         protected void onPreExecute() {
             super.onPreExecute();
             dialog = new Dialog(mContext);
             dialog.setCancelable(false);
             dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
             dialog.setContentView(R.layout.progressdialog);

             progressBar = (ProgressBar) dialog.findViewById(R.id.progressBar1);
             tvLoading = (TextView) dialog.findViewById(R.id.tv1);
             tvLoading.setText("Download VDO ใช้เวลาสักครู่ สามารถปิดหน้าจอเพื่อทำงานอื่นได้");
             tvPer = (TextView) dialog.findViewById(R.id.tvper);
             btnCancel = (Button) dialog.findViewById(R.id.btncancel);
             //btnCancel.setVisibility(View.GONE);
             btnCancel.setText("ปิด");
             btnCancel.setOnClickListener(new View.OnClickListener() {

                 @Override
                 public void onClick(View v) {
                    dialog.cancel();
                     finish();
                 }
             });

             dialog.show();
         }

         /**/
         @Override
         protected void onProgressUpdate(Integer... values) {
             super.onProgressUpdate(values);
             progressBar.setProgress(values[0]);
             tvLoading.setText("Loading...  " + values[0] + " %");
             tvPer.setText(values[0] + " %");
         }

         @Override
         protected void onPostExecute(Void result) {
             super.onPostExecute(result);

             dialog.dismiss();

             Log.i("URL --= aaaa =--", str.toString());
             finish();
         }
         public void setfile(String file) {
             myfile = file;
         }
     }
 }
