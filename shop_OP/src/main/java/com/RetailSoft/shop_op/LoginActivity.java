package com.RetailSoft.shop_op;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.RetailSoft.shop_op.util.BitmapUtil;
import com.RetailSoft.shop_op.util.NetworkUtil;
import com.RetailSoft.shop_op.util.PackageInfoManager;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends Activity {

	private LoginActivity mContext;
	private Button mLoginbtn;
	private TextView mEdittext;
	private MregTask mTask;
	private String qrStr;
	private String deviceIP;
	public NetworkUtil nwUtil;
	private IPRegTask mIPRegTask;
	public Bitmap bm;
	public BitmapUtil mBmUtil;
	public SharedPreferences prefs;
	private ProgressBar mProgress;
	private ImageView mVVbg;
	private String defBg;
	private BmpFetchTask mBMPTask;
	private String shotcutCreate;
	private CheckUpdate checkUpdateTask;
	private ProgressDialog mProgressDialog;
	private Intent intent;
	private String url_server="http://mshop.ssup.co.th/download/qrscaner.apk";
	
	PackageInfoManager pim;
	private TextView mAppname;
	

	protected void onCreate(Bundle savedInstanceState) {
		mContext = this;
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);

		pim = new PackageInfoManager(mContext);
		checkUpdateTask = new CheckUpdate();
		checkUpdateTask.execute();
		mBmUtil = new BitmapUtil();
		nwUtil = new NetworkUtil();

		deviceIP = chkWIFIConnect();

//		Intent serviceIntent = new Intent(mContext,GPSTracker.class);
//		serviceIntent.setAction("com.RetailSoft.shop_op;");
//		startService(serviceIntent);

		bindWidget();
		setListener();
		
		PackageInfo pInfo;
		try {
			pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			mAppname.setText(mAppname.getText()+" : "+pInfo.versionName);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
		defBg = prefs.getString("defWall", "");
		shotcutCreate = prefs.getString("shotcut", "No");

		Editor editor = prefs.edit();
		editor.putString("mobileIP", deviceIP);
		editor.commit();

		if (shotcutCreate.equalsIgnoreCase("No")) {
			createShotcut();
		}

		if (defBg.equalsIgnoreCase("")) {

		} else {
			bm = mBmUtil.StringToBitMap(defBg);
			mVVbg.setImageBitmap(bm);
		}
		mBMPTask = new BmpFetchTask();
		mBMPTask.execute();
	}

	private void createShotcut() {
		// TODO Auto-generated method stub
		Log.i("Create Shotcut", "proces");
		Editor editor = prefs.edit();
		editor.putString("shotcut", "Yes");
		editor.commit();

		Intent shortcutIntent = new Intent(mContext, LoginActivity.class);

		shortcutIntent.setAction(Intent.ACTION_MAIN);

		Intent addIntent = new Intent();
		addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
		addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, R.string.app_name);
		addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
				Intent.ShortcutIconResource.fromContext(
						getApplicationContext(), R.drawable.ic_launcher));

		addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
		mContext.sendBroadcast(addIntent);

	}

	private String chkWIFIConnect() {
		// TODO Auto-generated method stub
		String tmp = NetworkUtil.getIPAddress(true);
		return tmp;
	}

	private void setListener() {
		// TODO Auto-generated method stub

		mLoginbtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String apkName = "com.google.zxing.client.android";
				final AlertDialog.Builder ad = new AlertDialog.Builder(mContext);
				 boolean Installed = isAppInstalled(apkName);
					//final AlertDialog.Builder ad = new AlertDialog.Builder(this); 	
					if(Installed){

						intent = new Intent(
								"com.google.zxing.client.android.SCAN");
						intent.setPackage(apkName);
						intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
						startActivityForResult(intent, 0);
					}else{
						ad.setTitle("กรุณาติดตั้งแอปพลิเคชัน QR Scaner");
						ad.setPositiveButton("Close", null);
					    ad.setMessage("Please Download Application");
					    ad.show();
						intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url_server));
						startActivity(intent);
					}
				

			}
		});

	}

	private void bindWidget() {
		// TODO Auto-generated method stub
		mLoginbtn = (Button) findViewById(R.id.crmLoginBtn);
		mEdittext = (TextView) findViewById(R.id.idEdittext);
		mProgress = (ProgressBar) findViewById(R.id.progressBar1);
		mAppname = (TextView) findViewById(R.id.appnameshow);
		mVVbg = (ImageView) findViewById(R.id.vvbg);
		
	}

	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == 0) {
			if (resultCode == RESULT_OK) {
				String contents = intent.getStringExtra("SCAN_RESULT");
				// String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
				Log.i("result", contents);

				// mEdittext.setText(contents);
				qrStr = contents;
				mTask = new MregTask();
				mTask.execute();

				// Handle successful scan
				/*
				 * Intent mIntent = new Intent(mContext, MainActivity.class);
				 * mContext.startActivity(mIntent); /*
				 */
			} else if (resultCode == RESULT_CANCELED) {
				// Handle cancel
			}
		}
	}

	class MregTask extends AsyncTask<String, Integer, Void> {

		private Dialog dialog;
		private ProgressBar progressBar;
		private TextView tvLoading;
		private TextView tvPer;
		private Button btnCancel;
		private StringBuilder str;
		private DefaultHttpClient client;
		private HttpPost httpPost;
		private JSONObject jsonObj;

		@Override
		protected Void doInBackground(String... arg0) {
			// TODO Auto-generated method stub
			str = new StringBuilder();
			List<NameValuePair> prams = new ArrayList<NameValuePair>();
			prams.add(new BasicNameValuePair("uid", qrStr));
			prams.add(new BasicNameValuePair("mobileIP", deviceIP));

			String url = "http://mshop.ssup.co.th/shop_op/ch_user.php";
			// String url = "http://10.100.53.68/shop_op/ch_user.php";
			Log.i("URL Login", url + "?uid=" + qrStr + "&mobileIP=" + deviceIP);
			client = new DefaultHttpClient();
			httpPost = new HttpPost(url);
			try {
				httpPost.setEntity(new UrlEncodedFormEntity(prams));
				HttpResponse response = client.execute(httpPost);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) { // Status OK
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						str.append(line);
					}
				} else {
					Log.e("Log", "Failed to connect server..");
				}
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			Log.i("--= aaaa =--", str.toString());
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new Dialog(mContext);
			dialog.setCancelable(false);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.progressdialog);

			progressBar = (ProgressBar) dialog.findViewById(R.id.progressBar1);
			tvLoading = (TextView) dialog.findViewById(R.id.tv1);
			tvPer = (TextView) dialog.findViewById(R.id.tvper);
			btnCancel = (Button) dialog.findViewById(R.id.btncancel);

			btnCancel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					mTask.cancel(true);
					dialog.dismiss();
				}
			});

			dialog.show();
		}

		/**/
		@Override
		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
			progressBar.setProgress(values[0]);
			tvLoading.setText("Loading...  " + values[0] + " %");
			tvPer.setText(values[0] + " %");
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);

			dialog.dismiss();
			Log.i("Login", "Login");
			try {
				jsonObj = new JSONObject(str.toString());
				Log.i("login data", str.toString());
				String login_status = jsonObj.getString("login_status");
				String msg = jsonObj.getString("msg");

				/******* Save Shop ID Data *******/
				Editor editor = prefs.edit();
				editor.putString("shopName", jsonObj.getString("shopName"));
				editor.commit();
				/********************************/

				if (login_status.equalsIgnoreCase("OK")) {
					Log.i("Start IP REg", "Login");

					mIPRegTask = new IPRegTask();
					mIPRegTask.execute();

				} else {
					Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	class BmpFetchTask extends AsyncTask<String, Integer, Void> {

		private InputStream is;
		private BufferedInputStream bis;

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);

			String tmpBm = mBmUtil.BitMapToString(bm);
			Editor editor = prefs.edit();
			editor.putString("defWall", tmpBm);
			editor.commit();
			mProgress.setVisibility(View.GONE);
			mVVbg.setImageBitmap(bm);

			// bm = null;
		}

		@Override
		protected Void doInBackground(String... params) {
			// TODO Auto-generated method stub
			String url = "http://mshop.ssup.co.th/shop_op/desktop.jpg";
			// String url = "http://10.100.53.68/shop_op/ch_user.php";
			Log.i("execute", "process");
			try {
				URLConnection conn = new URL(url).openConnection();
				conn.connect();
				is = conn.getInputStream();
				bis = new BufferedInputStream(is, 8192);
				bm = BitmapFactory.decodeStream(bis);
			} catch (Exception e) {
				e.printStackTrace();
			}
			Log.i("finish load", "data" + bm);
			return null;
		}

	}

	class IPRegTask extends AsyncTask<String, Integer, Void> {

		private StringBuilder str;
		private DefaultHttpClient client;
		private HttpPost httpPost;
		private JSONObject jsonObj;

		@Override
		protected Void doInBackground(String... arg0) {
			// TODO Auto-generated method stub
			str = new StringBuilder();
			List<NameValuePair> prams = new ArrayList<NameValuePair>();
			prams.add(new BasicNameValuePair("uid", qrStr));
			prams.add(new BasicNameValuePair("mobileIP", deviceIP));

			String url = "http://mshop.ssup.co.th/shop_op/ipreg.php";
			client = new DefaultHttpClient();
			httpPost = new HttpPost(url);
			try {
				httpPost.setEntity(new UrlEncodedFormEntity(prams));
				HttpResponse response = client.execute(httpPost);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) { // Status OK
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						str.append(line);
					}
				} else {
					Log.e("Log", "Failed to connect server..");
				}
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			Log.i("IPRegData", str.toString());
			try {
				jsonObj = new JSONObject(str.toString());
				String regis_status = jsonObj.getString("ip_status");
				String msg = jsonObj.getString("msg");

				if (regis_status.equalsIgnoreCase("OK")) {
					Editor editor = prefs.edit();
					editor.putString("shopid", msg);
					editor.putString("userLogin", qrStr);
					editor.commit();

					Intent mIntent = new Intent(mContext, MainActivity.class);
					mContext.startActivity(mIntent);
				} else {
					Editor editor = prefs.edit();
					editor.putString("shopid", "7777");
					editor.putString("userLogin", "00a791");
					editor.commit();

					Intent mIntent = new Intent(mContext, MainActivity.class);
					mContext.startActivity(mIntent);

					Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	class CheckUpdate extends AsyncTask<String, Integer, Void> {

		private StringBuilder str;
		private DefaultHttpClient client;
		private HttpPost httpPost;
		private Boolean x = true;

		@Override
		protected Void doInBackground(String... arg0) {

			String url = "http://mshop.ssup.co.th/ros/appversion.php";// API


			// by
			Log.i("update task","jjjjj");
			client = new DefaultHttpClient();
			str = new StringBuilder();

			// sets the post request as the resulting string
			List<NameValuePair> prams = new ArrayList<NameValuePair>();
			httpPost = new HttpPost(url);
			JSONObject data = new JSONObject();
			// httpPost = new HttpGet(url);
			try {
				// httpPost.setEntity(se);
				prams.add(new BasicNameValuePair("appname", "shop_op"));// supervisor
				int versionCode = pim.getVersionCode();
				prams.add(new BasicNameValuePair("vcode", String.valueOf(versionCode)));
				httpPost.setEntity(new UrlEncodedFormEntity(prams));
				HttpResponse response = client.execute(httpPost);
				StatusLine statusLine = response.getStatusLine();

				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) { // Status OK
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						str.append(line);
					}
				} else {
					Log.e("CheckUpdate", "Failed to connect server..");
				}

			} catch (Exception e) {
				x=false;
				Log.e("CheckUpdate", "Exception");
				//Toast.makeText(getApplicationContext(),"kkkkkkkkk", Toast.LENGTH_LONG).show();

			}

			// Log.i("app data",str.toString());
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if(x==false) {
				//Toast.makeText(getApplicationContext(), "Check IP Wifi", Toast.LENGTH_LONG).show();
				Intent intent = new Intent(mContext,test2.class);
				startActivity(intent);
				finish();
			}
			try {
				Context superContext = getApplicationContext();
				JSONObject subObject = new JSONObject(str.toString());
				String newVersion = subObject.getString("newversion");
				String apVersion = subObject.getString("vname");
				String updateDetail = subObject.getString("update_desc");
				String url = subObject.getString("url");

				Log.i("update task","hhh");
				if(newVersion.equalsIgnoreCase("true")){
					Log.e("CheckUpdate:", "update");
					

					// instantiate it within the onCreate method
					mProgressDialog = new ProgressDialog(LoginActivity.this);
					mProgressDialog.setMessage("กำลังดาวโหลดเวอร์ชั่นใหม่");
					mProgressDialog.setIndeterminate(true);
					mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
					mProgressDialog.setCancelable(true);

					// execute this when the downloader must be fired
					final DownloadTask downloadTask = new DownloadTask(LoginActivity.this);
					downloadTask.execute(url);

					mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
					    @Override
					    public void onCancel(DialogInterface dialog) {
					        downloadTask.cancel(true);
					    }
					});
					
				}else{
					Log.e("CheckUpdate:", "no update");
				}
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
	
	
	private class DownloadTask extends AsyncTask<String, Integer, String> {

	    private Context context;
	    private PowerManager.WakeLock mWakeLock;

	    public DownloadTask(Context context) {
	        this.context = context;
	    }

	    @Override
	    protected String doInBackground(String... sUrl) {
	        InputStream input = null;
	        OutputStream output = null;
	        HttpURLConnection connection = null;
	        try {
	            URL url = new URL(sUrl[0]);
	            connection = (HttpURLConnection) url.openConnection();
	            connection.connect();

	            // expect HTTP 200 OK, so we don't mistakenly save error report
	            // instead of the file
	            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
	                return "Server returned HTTP " + connection.getResponseCode()
	                        + " " + connection.getResponseMessage();
	            }

	            // this will be useful to display download percentage
	            // might be -1: server did not report the length
	            int fileLength = connection.getContentLength();

	            // download the file
	            input = connection.getInputStream();
	            output = new FileOutputStream("/sdcard/newShop_op.apk");
	            

	            byte data[] = new byte[4096];
	            long total = 0;
	            int count;
	            while ((count = input.read(data)) != -1) {
	                // allow canceling with back button
	                if (isCancelled()) {
	                    input.close();
	                    return null;
	                }
	                total += count;
	                // publishing the progress....
	                if (fileLength > 0) // only if total length is known
	                    publishProgress((int) (total * 100 / fileLength));
	                output.write(data, 0, count);
	            }
	        } catch (Exception e) {
	            return e.toString();
	        } finally {
	            try {
	                if (output != null)
	                    output.close();
	                if (input != null)
	                    input.close();
	            } catch (IOException ignored) {
	            }

	            if (connection != null)
	                connection.disconnect();
	        }
	        return null;
	    }
	    
	    @Override
	    protected void onPreExecute() {
	        super.onPreExecute();
	        // take CPU lock to prevent CPU from going off if the user 
	        // presses the power button during download
	        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
	        mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
	             getClass().getName());
	        mWakeLock.acquire();
	        mProgressDialog.show();
	    }

	    @Override
	    protected void onProgressUpdate(Integer... progress) {
	        super.onProgressUpdate(progress);
	        // if we get here, length is known, now set indeterminate to false
	        mProgressDialog.setIndeterminate(false);
	        mProgressDialog.setMax(100);
	        mProgressDialog.setProgress(progress[0]);
	    }

	    @Override
	    protected void onPostExecute(String result) {
	        mWakeLock.release();
	        mProgressDialog.dismiss();
	        if (result != null)
	            Toast.makeText(context,"Download error: "+result, Toast.LENGTH_LONG).show(); //
	        else{
	        	//Toast.makeText(context,"File downloaded", Toast.LENGTH_SHORT).show();
	        	 Intent intent = new Intent(Intent.ACTION_VIEW);
	             intent.setAction(android.content.Intent.ACTION_VIEW);
	             intent.setDataAndType(Uri.parse("file:///sdcard/newShop_op.apk"), "application/vnd.android.package-archive");
	             
	             startActivity(intent);
	             
	             finish();
	        }
	            
	    }
	    
	}//DownloadTask
	
	 protected boolean isAppInstalled(String packageName) {
	        Intent mIntent = getPackageManager().getLaunchIntentForPackage(packageName);
	        if (mIntent != null) {
	            return true;
	        }
	        else {
	            return false;
	        }
	  }

	

}
