package com.RetailSoft.shop_op;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;

import com.RetailSoft.shop_op.util.JSONVideoFeeder;
import com.RetailSoft.shop_op.util.NetworkUtil;
import com.RetailSoft.shop_op.util.OnJSONReceiver;
import com.RetailSoft.shop_op.util.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Created by IS-PATTHANAPONG on 04/04/2559.
 */
public class TrainVideo extends ListActivity implements OnJSONReceiver {


    private static TrainVideo mContext;

    private boolean hasLabels = true;
    private boolean hasLabelsOutside = false;
    private boolean hasCenterCircle = true;
    private boolean hasCenterText1 = true;
    private boolean hasCenterText2 = true;
    private boolean isExploded = false;
    private boolean hasLabelForSelected = false;
    private static List<HashMap<String, String>> mDataList;
    private EfficientAdapter adapter;
    private static String brand;
    private Utils mUtil;
    private SharedPreferences prefs;
    private String shopName;
    private String deviceIP;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.videolist);

        mContext = this;
        mUtil = new Utils();
        adapter = new EfficientAdapter(this);
        //generateData();
        prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        shopName = prefs.getString("shopName", "");
        brand = mUtil.getShopBrand(shopName).toLowerCase();
        deviceIP = chkWIFIConnect();
        displayTask();
    }

    public String chkWIFIConnect() {
        // TODO Auto-generated method stub
        String tmp = NetworkUtil.getIPAddress(true);
        return tmp;
    }
    @Override
    public void onReceiveData(ArrayList<HashMap<String, String>> result) {
        mDataList = result;
        setListAdapter(adapter);
    }


      private static class EfficientAdapter extends BaseAdapter {

        private LayoutInflater mInflater;


          public EfficientAdapter(Context context) {
            mInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            int _row = (int) mDataList.size();
            return _row;

        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;

            final int pos = position;

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.stdrow, null);
                holder = new ViewHolder();
                holder.clayout = (LinearLayout) convertView
                        .findViewById(R.id.clayout);
                holder.iButton = (Button) convertView.findViewById(R.id.appBtn);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            final String filename = mDataList.get(pos).get(JSONVideoFeeder.KEY_FILE);
            final String urlfile = mDataList.get(pos).get(JSONVideoFeeder.KEY_URL);
            Log.i("URL DATA",urlfile+filename);
            if(mDataList.get(pos).get(JSONVideoFeeder.KEY_FSTATUS).equalsIgnoreCase("yes")){
                holder.iButton.setText(mDataList.get(pos).get(JSONVideoFeeder.KEY_TITLE));
                holder.iButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        Intent mIntent = null;
                        mIntent = new Intent(mContext, PlayVideo.class);
                        mIntent.putExtra("filename", urlfile+filename);
                        mContext.startActivity(mIntent);
                    }
                });
            }else{
                holder.iButton.setText("Download : "+mDataList.get(pos).get(JSONVideoFeeder.KEY_TITLE));
                holder.iButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        Intent mIntent = null;
                        mIntent = new Intent(mContext, DownloadVideo.class);
                        String newfile = urlfile + filename;
                        mIntent.putExtra("filename", filename);
                        Log.i("URL DATASEND",newfile);
                        mIntent.putExtra("urlpost", urlfile.replace("mcs/learn/", "mcs/learn/download_media.php"));
                        mContext.startActivity(mIntent);
                    }
                });
            }





			/**/
            return convertView;

        }

        static class ViewHolder {
            Button iButton;
            LinearLayout clayout;
        }

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        displayTask();
    }

    private void displayTask() {
        // TODO Auto-generated method stub
        // Log.i("data", feq + " :: " + type + "::"+shopName);
        JSONVideoFeeder.setData(brand,deviceIP);
        JSONVideoFeeder.getData(mContext);
    }


}
