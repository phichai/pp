package com.RetailSoft.shop_op;

import android.os.Environment;


public class ShopGlobalVariable {
  public static final String SDCARD_PATH = Environment.getExternalStorageDirectory().getAbsolutePath();
  public static final String FOLDER_NAME = "/" + Environment.DIRECTORY_DOWNLOADS;
  public static final String IMG_PATH = SDCARD_PATH + FOLDER_NAME +"/Shop_imgtmp";
  public static final String DB_PATH = SDCARD_PATH + FOLDER_NAME +"/ShopDB";
  private static final String DATABASE_NAME = "ShopOP.db";
  public static final String DATABASE_NAME_PATH = DB_PATH
			+ "/"+DATABASE_NAME ; 
	
}
