package com.RetailSoft.shop_op.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.PowerManager;
import android.widget.Toast;

import com.RetailSoft.shop_op.LoginActivity;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class PackageInfoManager {

	PackageInfo pInfo;
	Context context;
	ProgressDialog mProgressDialog;
	public PackageInfoManager(Context context){
		this.context = context;
		try {
			pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public String getVersionName() throws NameNotFoundException{
		return pInfo.versionName;
	}
	
	public int getVersionCode() throws NameNotFoundException{
		return pInfo.versionCode;
	}
	
	public Intent openBoxControl(){
		Intent i;
		PackageManager manager = context.getPackageManager();
		try {
		    i = manager.getLaunchIntentForPackage("com.barcode.android");
		    if (i == null)
		        throw new PackageManager.NameNotFoundException();
		    i.addCategory(Intent.CATEGORY_LAUNCHER);
		    return i;
		} catch (PackageManager.NameNotFoundException e) {
			String url = "http://mshop.ssup.co.th/download/myapp/boxcontrol.apk";
			
			mProgressDialog = new ProgressDialog(context);
			mProgressDialog.setMessage("กำลังดาว์นโหลดแอพพลิเคชั่น boxcontrol");
			mProgressDialog.setIndeterminate(true);
			mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			mProgressDialog.setCancelable(true);

			
			DownloadBoxControlTask downloadTask = new DownloadBoxControlTask(context);
			downloadTask.execute(url);
			i = new Intent(context, LoginActivity.class);
			return i;
		}
		
	}
	
	
	
	private class DownloadBoxControlTask extends AsyncTask<String, Integer, String> {

	    private Context context;
	    private PowerManager.WakeLock mWakeLock;

	    public DownloadBoxControlTask(Context context) {
	        this.context = context;
	    }

	    @Override
	    protected String doInBackground(String... sUrl) {
	        InputStream input = null;
	        OutputStream output = null;
	        HttpURLConnection connection = null;
	        try {
	            URL url = new URL(sUrl[0]);
	            connection = (HttpURLConnection) url.openConnection();
	            connection.connect();

	            // expect HTTP 200 OK, so we don't mistakenly save error report
	            // instead of the file
	            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
	                return "Server returned HTTP " + connection.getResponseCode()
	                        + " " + connection.getResponseMessage();
	            }

	            // this will be useful to display download percentage
	            // might be -1: server did not report the length
	            int fileLength = connection.getContentLength();

	            // download the file
	            input = connection.getInputStream();
	            output = new FileOutputStream("/sdcard/boxcontrol.apk");
	            

	            byte data[] = new byte[4096];
	            long total = 0;
	            int count;
	            while ((count = input.read(data)) != -1) {
	                // allow canceling with back button
	                if (isCancelled()) {
	                    input.close();
	                    return null;
	                }
	                total += count;
	                // publishing the progress....
	                if (fileLength > 0) // only if total length is known
	                    publishProgress((int) (total * 100 / fileLength));
	                output.write(data, 0, count);
	            }
	        } catch (Exception e) {
	            return e.toString();
	        } finally {
	            try {
	                if (output != null)
	                    output.close();
	                if (input != null)
	                    input.close();
	            } catch (IOException ignored) {
	            }

	            if (connection != null)
	                connection.disconnect();
	        }
	        return null;
	    }
	    
	    @Override
	    protected void onPreExecute() {
	        super.onPreExecute();
	        // take CPU lock to prevent CPU from going off if the user 
	        // presses the power button during download
	        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
	        mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
	             getClass().getName());
	        mWakeLock.acquire();
	        mProgressDialog.show();
	    }

	    @Override
	    protected void onProgressUpdate(Integer... progress) {
	        super.onProgressUpdate(progress);
	        // if we get here, length is known, now set indeterminate to false
	        mProgressDialog.setIndeterminate(false);
	        mProgressDialog.setMax(100);
	        mProgressDialog.setProgress(progress[0]);
	    }

	    @Override
	    protected void onPostExecute(String result) {
	        mWakeLock.release();
	        mProgressDialog.dismiss();
	        if (result != null)
	            Toast.makeText(context,"Download error: "+result, Toast.LENGTH_LONG).show();
	        else{
	        	//Toast.makeText(context,"File downloaded", Toast.LENGTH_SHORT).show();
	        	 Intent intent = new Intent(Intent.ACTION_VIEW);
	             intent.setAction(android.content.Intent.ACTION_VIEW);
	             intent.setDataAndType(Uri.parse("file:///sdcard/boxcontrol.apk"), "application/vnd.android.package-archive");
	             
	             context.startActivity(intent);
	        }
	            
	    }
	    
	}//DownloadTask
}
