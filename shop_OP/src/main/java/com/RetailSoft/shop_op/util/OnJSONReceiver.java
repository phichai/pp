package com.RetailSoft.shop_op.util;

import java.util.ArrayList;
import java.util.HashMap;

public interface OnJSONReceiver {
	  public void onReceiveData(ArrayList<HashMap<String, String>> result);
	}
