package com.RetailSoft.shop_op.util;

import android.os.Handler;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class JSONChklistFeeder {
	// XML node keys
	static final String KEY_ITEM = "ev"; // parent node

	public static final String KEY_NAME = "chklist_name";
	public static final String KEY_TRANS = "chklist_trans";
	public static final String KEY_ID = "id";
	public static final String KEY_PASS = "chklist_pass";
	public static final String KEY_ROSPASS = "chklist_got";
	public static final String KEY_ROSCOMMENT = "chklist_desc";

	private static String mShop;
	private static String mTaskType;
	private static String mType;

	public static void setQstr(String shop,String tasktype,String a) {
		mShop = shop;
		mTaskType = tasktype;
		mType = a;
	}
	


	public static void getData(final OnJSONReceiver delegate) {
		final Handler _handler = new Handler();

		new Thread(new Runnable() {

			private DefaultHttpClient client;
			private HttpPost httpPost;
			private StringBuilder str;
			private JSONArray jObject;

			@Override
			public void run() {

				str = new StringBuilder();
				// TODO Auto-generated method stub
				final ArrayList<HashMap<String, String>> itemList = new ArrayList<HashMap<String, String>>();

				List<NameValuePair> prams = new ArrayList<NameValuePair>();

				prams.add(new BasicNameValuePair("shop", mShop));
				prams.add(new BasicNameValuePair("chk_type", mTaskType));
				prams.add(new BasicNameValuePair("opr_type", mType));
				/**/
				Log.i("shop", mShop);
				Log.i("chk_type", mTaskType);
				Log.i("opr_type", mType);
				
				String url = "http://mshop.ssup.co.th/shop_op/shoptasklist.php";
				client = new DefaultHttpClient();
				httpPost = new HttpPost(url);
				try {
					httpPost.setEntity(new UrlEncodedFormEntity(prams));
					HttpResponse response = client.execute(httpPost);
					StatusLine statusLine = response.getStatusLine();
					int statusCode = statusLine.getStatusCode();
					if (statusCode == 200) { // Status OK
						HttpEntity entity = response.getEntity();
						InputStream content = entity.getContent();
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(content));
						String line;
						while ((line = reader.readLine()) != null) {
							str.append(line);
						}
						Log.i("JSON DATA", str.toString());
						jObject = new JSONArray(str.toString());
						for (int i = 0; i < jObject.length(); i++) {
							HashMap<String, String> map = new HashMap<String, String>();
							JSONObject subObject = jObject.getJSONObject(i);

							map.put(KEY_NAME, subObject.getString(KEY_NAME));
							map.put(KEY_TRANS, subObject.getString(KEY_TRANS));
							map.put(KEY_ID,subObject.getString(KEY_ID));
							map.put(KEY_PASS,subObject.getString(KEY_PASS));
							map.put(KEY_ROSPASS,subObject.getString(KEY_ROSPASS));
							map.put(KEY_ROSCOMMENT,subObject.getString(KEY_ROSCOMMENT));
							itemList.add(map);
						}

					} else {
						Log.e("Log", "Failed to connect server..");
					}
				} catch (ClientProtocolException e) {
					// Log.e("Log", "Failed to connect server..");
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					Log.e("Json error", "JSON Error");
					e.printStackTrace();
				}

				_handler.post(new Runnable() {

					@Override
					public void run() {
						delegate.onReceiveData(itemList);
					}
				});

			}
			/**/
		}).start();
	}

}