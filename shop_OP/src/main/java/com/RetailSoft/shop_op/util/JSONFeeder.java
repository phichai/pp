package com.RetailSoft.shop_op.util;

import android.os.Handler;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class JSONFeeder {
	// XML node keys
	static final String KEY_ITEM = "ev"; // parent node
	public static final String KEY_NAME = "name";
	public static final String KEY_LNAME = "surname";
	public static final String KEY_TEL = "mobile_no";
	public static final String KEY_MEMNO = "member_no";
	public static final String KEY_DATETEXT = "call_lastdate";
	public static final String KEY_CALLSTATUS = "call_flag";
	public static final String KEY_CALLCOUNT = "call_count";
	public static final String KEY_PROMO = "promo";
	public static final String KEY_CALLID = "call_id";
	public static final String KEY_CALLROUND = "send_round";
	public static final String KEY_BRAND = "brand_id";
	
	private static String mShop;
	private static String mFlag;
	private static String mCallType;

	public static void setQstr(String shop,String flag,String calltype) {
		mShop = shop;
		mFlag = flag;
		mCallType = calltype;
	}

	public static void getData(final OnJSONReceiver delegate) {
		final Handler _handler = new Handler();

		new Thread(new Runnable() {

			private DefaultHttpClient client;
			private HttpPost httpPost;
			private StringBuilder str;
			private JSONArray jObject;

			@Override
			public void run() {
				
				str = new StringBuilder();
				// TODO Auto-generated method stub
				final ArrayList<HashMap<String, String>> itemList = new ArrayList<HashMap<String, String>>();

				List<NameValuePair> prams = new ArrayList<NameValuePair>();
				//prams.add(new BasicNameValuePair("shop", "6123"));
				prams.add(new BasicNameValuePair("shop", mShop));
				prams.add(new BasicNameValuePair("callflag", mFlag));
				prams.add(new BasicNameValuePair("calltype", mCallType));
				Log.i("data","shop:"+mShop+" callflag:"+mFlag+" calltype:"+mCallType);
				//String url = "http://mobile.orientalprincess.com/shop/bdaymem.php";
				String url = "http://mshop.ssup.co.th/shop_op/bdaymem.php";
				client = new DefaultHttpClient();
				httpPost = new HttpPost(url);
				try {
					httpPost.setEntity(new UrlEncodedFormEntity(prams));
					HttpResponse response = client.execute(httpPost);
					StatusLine statusLine = response.getStatusLine();
					int statusCode = statusLine.getStatusCode();
					if (statusCode == 200) { // Status OK
						HttpEntity entity = response.getEntity();
						InputStream content = entity.getContent();
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(content));
						String line;
						while ((line = reader.readLine()) != null) {
							str.append(line);
						}
						Log.i("Mass JSON",str.toString());
						jObject = new JSONArray(str.toString());
						for (int i = 0; i < jObject.length(); i++) {
							HashMap<String, String> map = new HashMap<String, String>();
							JSONObject subObject = jObject.getJSONObject(i);
							//Log.i("json Data",subObject.toString());
							map.put(KEY_NAME,subObject.getString(KEY_NAME));
							map.put(KEY_BRAND,subObject.getString(KEY_BRAND));
							map.put(KEY_LNAME,subObject.getString(KEY_LNAME));
							map.put(KEY_MEMNO,subObject.getString(KEY_MEMNO));
							
							map.put(KEY_TEL, subObject.getString(KEY_TEL));
							map.put(KEY_CALLCOUNT, subObject.getString(KEY_CALLCOUNT));
							
							Log.i("BRAND",subObject.getString(KEY_BRAND));
							
							if(subObject.getString(KEY_BRAND).equalsIgnoreCase("OP")){
								map.put(KEY_PROMO, subObject.getString("promo_des"));
							}else{
								map.put(KEY_PROMO, subObject.getString("promo_code")+":"+subObject.getString("promo_net"));
							}
							
							if(subObject.getString(KEY_CALLCOUNT).equalsIgnoreCase("0")){
								map.put(KEY_DATETEXT,"ยังไม่มีการโทร");
							}else{
								map.put(KEY_DATETEXT,subObject.getString(KEY_DATETEXT));
							}
							
							
							
							map.put(KEY_CALLID, subObject.getString("send_year")+"_"+subObject.getString("send_month"));
							map.put(KEY_CALLROUND, subObject.getString(KEY_CALLROUND));
							
							itemList.add(map);
						}

					} else {
						Log.e("Log", "Failed to connect server..");
					}
				} catch (ClientProtocolException e) {
					// Log.e("Log", "Failed to connect server..");
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					Log.e("Json error","JSON Error");
					e.printStackTrace();
				}finally{
					Log.i("Mass JSON",str.toString());
				}

				_handler.post(new Runnable() {

					@Override
					public void run() {
						delegate.onReceiveData(itemList);
					}
				});

			}
			/**/
		}).start();
	}

}