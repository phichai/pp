-- Merging decision tree log ---
manifest
ADDED from AndroidManifest.xml:2:1
	xmlns:android
		ADDED from AndroidManifest.xml:2:11
	package
		ADDED from AndroidManifest.xml:3:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:versionName
		ADDED from AndroidManifest.xml:5:5
	android:versionCode
		ADDED from AndroidManifest.xml:4:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
uses-permission#android.permission.READ_PHONE_STATE
ADDED from AndroidManifest.xml:6:5
	android:name
		ADDED from AndroidManifest.xml:6:22
uses-permission#android.permission.INTERNET
ADDED from AndroidManifest.xml:7:5
	android:name
		ADDED from AndroidManifest.xml:7:22
uses-permission#android.permission.ACCESS_NETWORK_STATE
ADDED from AndroidManifest.xml:8:5
	android:name
		ADDED from AndroidManifest.xml:8:22
uses-permission#android.permission.READ_CONTACTS
ADDED from AndroidManifest.xml:9:5
	android:name
		ADDED from AndroidManifest.xml:9:22
uses-permission#android.permission.READ_LOGS
ADDED from AndroidManifest.xml:10:5
	android:name
		ADDED from AndroidManifest.xml:10:22
uses-permission#android.permission.CALL_PHONE
ADDED from AndroidManifest.xml:11:5
	android:name
		ADDED from AndroidManifest.xml:11:22
uses-permission#com.android.launcher.permission.INSTALL_SHORTCUT
ADDED from AndroidManifest.xml:12:5
	android:name
		ADDED from AndroidManifest.xml:12:22
uses-permission#com.android.launcher.permission.UNINSTALL_SHORTCUT
ADDED from AndroidManifest.xml:13:5
	android:name
		ADDED from AndroidManifest.xml:13:22
uses-permission#android.permission.WRITE_EXTERNAL_STORAGE
ADDED from AndroidManifest.xml:14:5
	android:name
		ADDED from AndroidManifest.xml:14:22
uses-permission#android.permission.WAKE_LOCK
ADDED from AndroidManifest.xml:15:2
	android:name
		ADDED from AndroidManifest.xml:15:19
uses-sdk
ADDED from AndroidManifest.xml:16:5
	android:targetSdkVersion
		ADDED from AndroidManifest.xml:18:9
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:minSdkVersion
		ADDED from AndroidManifest.xml:17:9
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
application
ADDED from AndroidManifest.xml:20:5
	android:label
		ADDED from AndroidManifest.xml:23:9
	android:allowBackup
		ADDED from AndroidManifest.xml:21:9
	android:icon
		ADDED from AndroidManifest.xml:22:9
	android:theme
		ADDED from AndroidManifest.xml:24:9
activity#com.RetailSoft.shop_op.CrmTel
ADDED from AndroidManifest.xml:25:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:27:13
	android:name
		ADDED from AndroidManifest.xml:26:13
activity#com.RetailSoft.shop_op.LoginActivity
ADDED from AndroidManifest.xml:29:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:32:13
	android:exported
		ADDED from AndroidManifest.xml:31:13
	android:name
		ADDED from AndroidManifest.xml:30:13
intent-filter#android.intent.action.MAIN+android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:33:13
action#android.intent.action.MAIN
ADDED from AndroidManifest.xml:34:17
	android:name
		ADDED from AndroidManifest.xml:34:25
category#android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:36:17
	android:name
		ADDED from AndroidManifest.xml:36:27
activity#com.RetailSoft.shop_op.ShopProblem
ADDED from AndroidManifest.xml:39:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:41:13
	android:name
		ADDED from AndroidManifest.xml:40:13
activity#com.RetailSoft.shop_op.MainActivity
ADDED from AndroidManifest.xml:43:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:46:13
	android:label
		ADDED from AndroidManifest.xml:45:13
	android:name
		ADDED from AndroidManifest.xml:44:13
activity#com.RetailSoft.shop_op.OperationChecklist
ADDED from AndroidManifest.xml:48:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:51:13
	android:label
		ADDED from AndroidManifest.xml:50:13
	android:name
		ADDED from AndroidManifest.xml:49:13
activity#com.RetailSoft.shop_op.TaskDetail
ADDED from AndroidManifest.xml:53:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:56:13
	android:label
		ADDED from AndroidManifest.xml:55:13
	android:name
		ADDED from AndroidManifest.xml:54:13
activity#com.RetailSoft.shop_op.CustomTaskDetail
ADDED from AndroidManifest.xml:58:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:61:13
	android:label
		ADDED from AndroidManifest.xml:60:13
	android:name
		ADDED from AndroidManifest.xml:59:13
activity#com.RetailSoft.shop_op.CallPrepare
ADDED from AndroidManifest.xml:63:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:66:13
	android:label
		ADDED from AndroidManifest.xml:65:13
	android:name
		ADDED from AndroidManifest.xml:64:13
activity#com.RetailSoft.shop_op.playerstream
ADDED from AndroidManifest.xml:68:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:71:13
	android:label
		ADDED from AndroidManifest.xml:70:13
	android:name
		ADDED from AndroidManifest.xml:69:13
activity#com.RetailSoft.shop_op.PlayVideo
ADDED from AndroidManifest.xml:73:10
	android:screenOrientation
		ADDED from AndroidManifest.xml:76:13
	android:label
		ADDED from AndroidManifest.xml:75:13
	android:name
		ADDED from AndroidManifest.xml:74:13
activity#io.vov.vitamio.demo.VideoViewDemo
ADDED from AndroidManifest.xml:80:8
	android:screenOrientation
		ADDED from AndroidManifest.xml:82:13
	android:name
		ADDED from AndroidManifest.xml:81:13
receiver#com.RetailSoft.shop_op.PhoneStateReceiver
ADDED from AndroidManifest.xml:85:9
	android:name
		ADDED from AndroidManifest.xml:85:19
intent-filter#android.intent.action.PHONE_STATE
ADDED from AndroidManifest.xml:86:13
action#android.intent.action.PHONE_STATE
ADDED from AndroidManifest.xml:87:17
	android:name
		ADDED from AndroidManifest.xml:87:25
